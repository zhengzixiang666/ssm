package zzx.enumutil;

public interface BaseLongEnum {
	
	public abstract String getDisplayName();
	
	public abstract Long getValue();

}
