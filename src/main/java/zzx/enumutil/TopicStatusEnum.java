package zzx.enumutil;

public enum TopicStatusEnum implements BaseStringEnum{
	
	TOPIC_STATUS_1("1","未审核"),
	TOPIC_STATUS_2("2","已审核"),
	TOPIC_STATUS_3("3","审核未通过");
	
	public String value;
	
	public String dispalyName;
	
	private TopicStatusEnum(String value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}

	//通过code找名称
	//根据值获取枚举
	public static String getTopicStatusEnumByValue(String value){
		for (TopicStatusEnum topicStatusEnum : TopicStatusEnum.values()) {
			if(topicStatusEnum.value.equals(value)){
				return topicStatusEnum.dispalyName;
			}
		}
		return null;
	}
	
	@Override
	public String getDisplayName() {
		
		return this.dispalyName;
	}

	@Override
	public String getValue() {
		return this.value;
	}

}
