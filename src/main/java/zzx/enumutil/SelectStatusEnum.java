package zzx.enumutil;

public enum SelectStatusEnum implements BaseStringEnum{
	
	Select_STATUS_1("1","未选择"),
	Select_STATUS_2("2","审核通过"),
	Select_STATUS_3("3","未审核"),
	Select_STATUS_4("4","审核不通过");
	
	public String value;
	
	public String dispalyName;
	
	private SelectStatusEnum(String value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}

	//通过code找名称
	//根据值获取枚举
	public static String getSelectStatusEnumByValue(String value){
		for (SelectStatusEnum selectStatusEnum : SelectStatusEnum.values()) {
			if(selectStatusEnum.value.equals(value)){
				return selectStatusEnum.dispalyName;
			}
		}
		return null;
	}
	
	@Override
	public String getDisplayName() {
		
		return this.dispalyName;
	}

	@Override
	public String getValue() {
		return this.value;
	}
}
