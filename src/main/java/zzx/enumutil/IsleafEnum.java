package zzx.enumutil;

public enum IsleafEnum implements BaseIntegerEnum {
	ISLEAF_1(1,"功能"),
	ISLEAF_2(2,"菜单");
	
	public Integer value;
	
	public String dispalyName;
	
	//根据值获取枚举
	public static IsleafEnum getIsleafEnumByValue(String value){
		for (IsleafEnum isleafEnum : IsleafEnum.values()) {
			if(isleafEnum.value.equals(value)){
				return isleafEnum;
			}
		}
		return null;
	}
	//根据名称获取枚举
	public static IsleafEnum getIsleafEnumByName(String displayName){
		for (IsleafEnum isleafEnum : IsleafEnum.values()) {
			if(isleafEnum.dispalyName.equals(displayName)){
				return isleafEnum;
			}
		}
		return null;
	}
	
	
	private IsleafEnum(Integer value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}
	public String getDisplayName() {
		
		return this.dispalyName;
	}
	public Integer getValue() {
		return this.value;
	}

}
