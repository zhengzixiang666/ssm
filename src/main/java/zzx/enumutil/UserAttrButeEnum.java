package zzx.enumutil;

public enum UserAttrButeEnum implements BaseStringEnum {
	
	ADMIN("1","超级管理员"),
	
	GLY("2","管理员"),
	
	CZY("3","操作员");

	public String value;
	
	public String dispalyName;
	
	//根据值获取枚举
	public static UserAttrButeEnum getUserAttrButeEnumByValue(String value){
		for (UserAttrButeEnum userAttrButeEnum : UserAttrButeEnum.values()) {
			if(userAttrButeEnum.value.equals(value)){
				return userAttrButeEnum;
			}
		}
		return null;
	}
	//根据名称获取枚举
	public static UserAttrButeEnum getUserAttrButeEnumByName(String displayName){
		for (UserAttrButeEnum userAttrButeEnum : UserAttrButeEnum.values()) {
			if(userAttrButeEnum.dispalyName.equals(displayName)){
				return userAttrButeEnum;
			}
		}
		return null;
	}
	
	

	private UserAttrButeEnum(String value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}
	public String getDisplayName() {
		
		return this.dispalyName;
	}
	public String getValue() {
		return this.value;
	}

}
