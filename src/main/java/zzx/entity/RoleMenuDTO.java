package zzx.entity;

/**
 * 角色菜单关联表tur_role_menu
 * @author 郑仔祥
 *
 */
public class RoleMenuDTO {
	
	public Integer id;//角色菜单关联表主键
	
	public Integer roleid;//角色主键
	
	public Integer menuid;//菜单主键
	
	public RoleDTO roleDTO;//角色
	
	public MenuDTO menuDTO;//菜单
	
	public RoleMenuDTO(){}
	
	

	public RoleMenuDTO(Integer id, Integer roleid, Integer menuid, RoleDTO roleDTO, MenuDTO menuDTO) {
		this.id = id;
		this.roleid = roleid;
		this.menuid = menuid;
		this.roleDTO = roleDTO;
		this.menuDTO = menuDTO;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Integer getRoleid() {
		return roleid;
	}



	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}



	public Integer getMenuid() {
		return menuid;
	}



	public void setMenuid(Integer menuid) {
		this.menuid = menuid;
	}



	public RoleDTO getRoleDTO() {
		return roleDTO;
	}



	public void setRoleDTO(RoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}



	public MenuDTO getMenuDTO() {
		return menuDTO;
	}



	public void setMenuDTO(MenuDTO menuDTO) {
		this.menuDTO = menuDTO;
	}



	@Override
	public String toString() {
		return "RoleMenuDTO [id=" + id + ", roleid=" + roleid + ", menuid=" + menuid + ", roleDTO=" + roleDTO
				+ ", menuDTO=" + menuDTO + "]";
	}



	
	

	
	
	

}
