package zzx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@TableName("sys_apply")
public class ApplyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
       private Integer id;

    /**
     * 申请人
     */
    private Integer userId;

    /**
     * 请假类型
     */
    private String qjType;

    /**
     * 请假事由
     */
    private String qjDesc;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 申请状态
     */
    private String applyStatus;

    /**
     * 提交时间
     */
    private Date submitTime;
    
    /**
     * 开始时间
     */
    private String startTimeStr;

    /**
     * 结束时间
     */
    private String endTimeStr;
    
    /**
     * 提交时间
     */
    private String submitTimeStr;

    /**
     * 申请编号
     */
    private String qjCode;
    
    


    public String getStartTimeStr() {
		return startTimeStr;
	}

	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public String getSubmitTimeStr() {
		return submitTimeStr;
	}

	public void setSubmitTimeStr(String submitTimeStr) {
		this.submitTimeStr = submitTimeStr;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getQjType() {
        return qjType;
    }

    public void setQjType(String qjType) {
        this.qjType = qjType;
    }

    public String getQjDesc() {
        return qjDesc;
    }

    public void setQjDesc(String qjDesc) {
        this.qjDesc = qjDesc;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getQjCode() {
        return qjCode;
    }

    public void setQjCode(String qjCode) {
        this.qjCode = qjCode;
    }

    @Override
    public String toString() {
        return "ApplyDTO{" +
        "id=" + id +
        ", userId=" + userId +
        ", qjType=" + qjType +
        ", qjDesc=" + qjDesc +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", applyStatus=" + applyStatus +
        ", submitTime=" + submitTime +
        ", qjCode=" + qjCode +
        "}";
    }
}
