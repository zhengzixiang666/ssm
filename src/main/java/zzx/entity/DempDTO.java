package zzx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@TableName("sys_demp")
public class DempDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	/**
	 * 员工编号
	 */
	private String deCode;

	/**
	 * 员工姓名
	 */
	private String deName;

	/**
	 * 性别
	 */
	private String deSex;

	/**
	 * 所属部门
	 */
	private Integer dpId;

	/**
	 * 入职时间
	 */
	private Date deTime;

	/**
	 * 入职时间
	 */
	private String deTimeStr;
	
	

	public String getDeTimeStr() {
		return deTimeStr;
	}

	public void setDeTimeStr(String deTimeStr) {
		this.deTimeStr = deTimeStr;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeCode() {
		return deCode;
	}

	public void setDeCode(String deCode) {
		this.deCode = deCode;
	}

	public String getDeName() {
		return deName;
	}

	public void setDeName(String deName) {
		this.deName = deName;
	}

	public String getDeSex() {
		return deSex;
	}

	public void setDeSex(String deSex) {
		this.deSex = deSex;
	}

	public Integer getDpId() {
		return dpId;
	}

	public void setDpId(Integer dpId) {
		this.dpId = dpId;
	}

	public Date getDeTime() {
		return deTime;
	}

	public void setDeTime(Date deTime) {
		this.deTime = deTime;
	}

	@Override
	public String toString() {
		return "DempDTO{" + "id=" + id + ", deCode=" + deCode + ", deName=" + deName + ", deSex=" + deSex + ", dpId="
				+ dpId + ", deTime=" + deTime + "}";
	}
}
