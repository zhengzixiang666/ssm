package zzx.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;


public class SysProject implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 批次号
     */
    private String pch;

    /**
     * 类型名称
     */
    private String lxmc;

    /**
     * 逻辑号
     */
    private String ljh;

    /**
     * 用户地址
     */
    private String yhdz;

    /**
     * 施工人
     */
    private String sgr;

    /**
     * 施工电话
     */
    private String sgdh;

    /**
     * 片区
     */
    private String pq;

    /**
     * 故障原因
     */
    private String gzyy;

    /**
     * 光网络箱
     */
    private String gllx;

    /**
     * 日期
     */
    private Date cjrq;
    
    private String sort;
    

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPch() {
		return pch;
	}

	public void setPch(String pch) {
		this.pch = pch;
	}

	public String getLxmc() {
		return lxmc;
	}

	public void setLxmc(String lxmc) {
		this.lxmc = lxmc;
	}

	public String getLjh() {
		return ljh;
	}

	public void setLjh(String ljh) {
		this.ljh = ljh;
	}

	public String getYhdz() {
		return yhdz;
	}

	public void setYhdz(String yhdz) {
		this.yhdz = yhdz;
	}

	public String getSgr() {
		return sgr;
	}

	public void setSgr(String sgr) {
		this.sgr = sgr;
	}

	public String getSgdh() {
		return sgdh;
	}

	public void setSgdh(String sgdh) {
		this.sgdh = sgdh;
	}

	public String getPq() {
		return pq;
	}

	public void setPq(String pq) {
		this.pq = pq;
	}

	public String getGzyy() {
		return gzyy;
	}

	public void setGzyy(String gzyy) {
		this.gzyy = gzyy;
	}

	public String getGllx() {
		return gllx;
	}

	public void setGllx(String gllx) {
		this.gllx = gllx;
	}

	public Date getCjrq() {
		return cjrq;
	}

	public void setCjrq(Date cjrq) {
		this.cjrq = cjrq;
	}
    
    
}