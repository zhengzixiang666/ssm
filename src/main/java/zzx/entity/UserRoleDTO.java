package zzx.entity;

/**
 * 用户角色关联表tur_user_role
 * @author 郑仔祥
 *
 */
public class UserRoleDTO {
	
	public Integer id;//角色用户关联表主键
	
	public Integer roleid;//角色id
	
	public Integer userid;//用户id
	
	public UserDTO userDTO;//用户dto
	
	public RoleDTO roleDTO;//角色dto
	
	public UserRoleDTO(){}

	

	public UserRoleDTO(Integer id, Integer roleid, Integer userid, UserDTO userDTO, RoleDTO roleDTO) {
		this.id = id;
		this.roleid = roleid;
		this.userid = userid;
		this.userDTO = userDTO;
		this.roleDTO = roleDTO;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	

	public UserDTO getUserDTO() {
		return userDTO;
	}



	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}



	public RoleDTO getRoleDTO() {
		return roleDTO;
	}



	public void setRoleDTO(RoleDTO roleDTO) {
		this.roleDTO = roleDTO;
	}



	@Override
	public String toString() {
		return "UserRoleDTO [id=" + id + ", roleid=" + roleid + ", userid=" + userid + ", userDTO=" + userDTO
				+ ", roleDTO=" + roleDTO + "]";
	}

	

	
	

}
