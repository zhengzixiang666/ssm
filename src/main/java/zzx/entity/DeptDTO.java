package zzx.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@TableName("sys_dept")
public class DeptDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
       private Integer id;

    /**
     * 部门编号
     */
    private String dpCode;

    /**
     * 部门名称
     */
    private String dpName;

    /**
     * 部门位置
     */
    private String dpAddr;

    /**
     * 部门负责人
     */
    private String dpUsername;

    /**
     * 用户id
     */
    private Integer userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDpCode() {
        return dpCode;
    }

    public void setDpCode(String dpCode) {
        this.dpCode = dpCode;
    }

    public String getDpName() {
        return dpName;
    }

    public void setDpName(String dpName) {
        this.dpName = dpName;
    }

    public String getDpAddr() {
        return dpAddr;
    }

    public void setDpAddr(String dpAddr) {
        this.dpAddr = dpAddr;
    }

    public String getDpUsername() {
        return dpUsername;
    }

    public void setDpUsername(String dpUsername) {
        this.dpUsername = dpUsername;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "DeptDTO{" +
        "id=" + id +
        ", dpCode=" + dpCode +
        ", dpName=" + dpName +
        ", dpAddr=" + dpAddr +
        ", dpUsername=" + dpUsername +
        ", userId=" + userId +
        "}";
    }
}
