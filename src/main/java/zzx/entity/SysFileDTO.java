package zzx.entity;

import java.util.Date;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhengzixiang
 * @since 2019-02-28
 */
public class SysFileDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 用户名
     */
    private String loginCode;

    /**
     * 文件后缀名
     */
    private String suffixName;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件描述
     */
    private String message;

    /**
     * 上传时间
     */
    private Date createTime;
    
    private byte[] document;
    
    

    public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }
    public String getSuffixName() {
        return suffixName;
    }

    public void setSuffixName(String suffixName) {
        this.suffixName = suffixName;
    }
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SysFile{" +
        "id=" + id +
        ", loginCode=" + loginCode +
        ", suffixName=" + suffixName +
        ", fileType=" + fileType +
        ", fileName=" + fileName +
        ", message=" + message +
        ", createTime=" + createTime +
        "}";
    }
}
