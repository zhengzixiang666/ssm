package zzx.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@TableName("sys_bx")
public class BxDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
       private Integer id;

    /**
     * 报销编号
     */
    private String bxCode;

    /**
     * 申请人
     */
    private Integer userId;

    /**
     * 报销类型
     */
    private String bxType;

    /**
     * 金额
     */
    private String money;

    /**
     * 申请时间
     */
    private Date applyTime;
    
    /**
     * 申请时间
     */
    private String applyTimeStr;

    /**
     * 状态
     */
    private String applyStatus;

    /**
     * 摘要
     */
    private String bxDesc;
    
    


    public String getApplyTimeStr() {
		return applyTimeStr;
	}

	public void setApplyTimeStr(String applyTimeStr) {
		this.applyTimeStr = applyTimeStr;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBxCode() {
        return bxCode;
    }

    public void setBxCode(String bxCode) {
        this.bxCode = bxCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBxType() {
        return bxType;
    }

    public void setBxType(String bxType) {
        this.bxType = bxType;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getBxDesc() {
        return bxDesc;
    }

    public void setBxDesc(String bxDesc) {
        this.bxDesc = bxDesc;
    }

    @Override
    public String toString() {
        return "BxDTO{" +
        "id=" + id +
        ", bxCode=" + bxCode +
        ", userId=" + userId +
        ", bxType=" + bxType +
        ", money=" + money +
        ", applyTime=" + applyTime +
        ", applyStatus=" + applyStatus +
        ", bxDesc=" + bxDesc +
        "}";
    }
}
