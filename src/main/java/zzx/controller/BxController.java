package zzx.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import zzx.entity.BxDTO;
import zzx.entity.UserDTO;
import zzx.exception.HandException;
import zzx.service.BxService;
import zzx.service.UserService;
import zzx.util.CodeUtil;
import zzx.util.JsonUtil;

/**
 * <p>
 *  鍓嶇鎺у埗鍣�
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@Controller
@RequestMapping("/bxDTO")
public class BxController {

	@Autowired
    private BxService bxService;
    
	@Resource
	public UserService userService;
    
    /**
	 * 初始页面
	 * @return
	 */
	@RequestMapping("init")
	public ModelAndView init(HttpServletRequest request,ModelAndView mv){
		
		mv.setViewName("bxDTO/init");
		HttpSession session=request.getSession();
		Object obj=session.getAttribute("userId");
		if(obj!=null && !StringUtils.isBlank(obj.toString())) {
			Integer currentId=Integer.valueOf(obj.toString());
			//通过用户id查询员工id
			UserDTO user=userService.findUserDTOById(currentId);
			mv.addObject("user", user);
		}
		return mv;
	}
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listByCondition")
	@ResponseBody
	public JsonUtil listByCondition(BxDTO obj){
		List<BxDTO> list=bxService.selectListByDTO(obj);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	/**
	 * 新增的主页面
	 * @return
	 */
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "bxDTO/addMain";
	}
	
	/**
	 * 新增方法
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(BxDTO obj,HttpServletRequest request){
		JsonUtil json=new JsonUtil();
		try {
			obj.setApplyTime(new Date());
			if(StringUtils.isBlank(obj.getBxCode())) {
				obj.setBxCode(CodeUtil.getBXCode());
			}
			if(obj.getUserId()==null) {
				Object object=request.getSession().getAttribute("userId");
				if(object!=null && !StringUtils.isBlank(object.toString())) {
					Integer currentId=Integer.valueOf(object.toString());
					//通过用户id查询员工id
					UserDTO user=userService.findUserDTOById(currentId);
					obj.setUserId(user.getDempId());
				}
			}
			bxService.saveOne(obj);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"新增成功！",null);
	}
	
	/**
	 * 修改主页面
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("bxDTO/editMain");
		
		Map<String,Object>	map =new HashMap<>();
		try {
			BxDTO obj = bxService.selectById(id);
			SimpleDateFormat  f= new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			obj.setApplyTimeStr(f.format(obj.getApplyTime()));
			mv.addObject("obj",obj);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 查看主页面
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/searchMain")
	public ModelAndView searchMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("bxDTO/searchMain");
		
		Map<String,Object>	map =new HashMap<>();
		try {
			BxDTO obj = bxService.selectById(id);
			mv.addObject("obj",obj);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 修改方法
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public JsonUtil edit(BxDTO obj){
		JsonUtil json=new JsonUtil();
		try {
			bxService.updateById(obj);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 删除
	 * @param userId
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			bxService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
	@RequestMapping("/findEnumDTOByEnumWord")
	@ResponseBody
	public List<BxDTO> findEnumDTOByEnumWord(@RequestParam Integer enumWord){
		return bxService.selectListByDTO(null);
	} 
	
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listById")
	@ResponseBody
	public JsonUtil listById(BxDTO obj){
		BxDTO list=bxService.selectById(obj.getId());
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
    
}

