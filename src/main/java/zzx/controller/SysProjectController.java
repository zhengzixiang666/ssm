package zzx.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import zzx.entity.SysProject;
import zzx.entity.SysUserFileDTO;
import zzx.exception.HandException;
import zzx.service.SysProjectService;
import zzx.util.CodeUtil;
import zzx.util.DateTimeUtil;
import zzx.util.FileUtil;
import zzx.util.JsonUtil;
import zzx.util.ListUtil;

@Controller
@RequestMapping("sysProject")
public class SysProjectController {
	
	@Resource
	private SysProjectService sysProjectService;
	
	
	@RequestMapping("list")
	public String list(){
		return "sysProject/list";
	}
	
	/**
	 * 新增文件
	 * @return
	 */
	@RequestMapping("doAddUpload")
	public String doAddUpload(){
		return "sysProject/doAddUpload";
	}
	
	@RequestMapping("/select")
	@ResponseBody
	public JsonUtil findAllSysUserFileDTO(SysProject sysProject){
		List<SysProject>  roleDTOList=sysProjectService.selectListByDTO(sysProject);
		JsonUtil json=new JsonUtil(true,"发送成功",roleDTOList);
		return json;
	}
	
	@RequestMapping("doUploads")
	@ResponseBody
	public JsonUtil doUploads(HttpServletRequest request, @RequestParam(value="file1")MultipartFile file) throws FileNotFoundException{
		String fileName = request.getParameter("fileName");//文件名
		if(StringUtils.isBlank(fileName)) {
			fileName="用户地址";
		}
		String pch = request.getParameter("pch");//替换地址得批次号
		if(StringUtils.isBlank(pch)) {
			return new JsonUtil(false,"替换失败，批次号不能为空！！","");
		}
		String fileSuffixName=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));//获取文件后缀名
		if(!".xlsx".equals(fileSuffixName) && !".xls".equals(fileSuffixName)) {
			return new JsonUtil(false,"上传失败，文件类型不正确！！","");
		}
		try {
			sysProjectService.doImportExcel(file,fileSuffixName,pch);
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),"");
		}
        return new JsonUtil(true,"替换成功！","");
	}
	

	@RequestMapping(value="/doExcel")
	public void doExcel(HttpServletResponse res,HttpServletRequest req,SysProject sysProject) throws Exception{
		List<SysProject>  roleDTOList=sysProjectService.selectListByDTO(sysProject);
		List<String> list=new ArrayList<>();
		list.add("批次号");
		list.add("类型名称");
		list.add("逻辑号");
		list.add("用户地址");
		list.add("施工人");
		list.add("施工电话");
		list.add("片区");
		list.add("故障原因");
		list.add("光网络箱");
		list.add("日期");
		List<Map<Integer,Object>> list2=new ArrayList<Map<Integer,Object>>();
		if(!CollectionUtils.isEmpty(roleDTOList)) {
			for (SysProject sys : roleDTOList) {
				Map<Integer,Object> map=new HashMap<>();
				map.put(0, sys.getPch());
				map.put(1, sys.getLxmc());
				map.put(2, sys.getLjh());
				map.put(3, sys.getYhdz());
				map.put(4, sys.getSgr());
				map.put(5, sys.getSgdh());
				map.put(6, sys.getPq());
				map.put(7, sys.getGzyy());
				map.put(8, sys.getGllx());
				map.put(9, DateTimeUtil.formatDate("yyyy-MM-dd", sys.getCjrq()));
				list2.add(map);
			}
		}
		FileUtil.doExcel("清单",null, res,list, list2);
	}
	
	

}
