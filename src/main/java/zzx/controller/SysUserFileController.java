package zzx.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.SysUserFileDTO;
import zzx.exception.HandException;
import zzx.service.SysProjectService;
import zzx.service.SysUserFileService;
import zzx.util.CodeUtil;
import zzx.util.FileUtil;
import zzx.util.JsonUtil;
import zzx.util.ListUtil;

@Controller
@RequestMapping("userFile")
public class SysUserFileController {
	
	@Resource
	private SysProjectService sysProjectService;
	
	@Resource
	private SysUserFileService sysUserFileService;
	
	@RequestMapping("createFile")
	public String getCreateFile(){
		return "userFile/createFile";
	}
	
	
	
	/**
	 * 文件上传主页面
	 * @return
	 */
	@RequestMapping("upload")
	public String upload(){
		return "userFile/upload";
	}
	
	/**
	 * 新增文件
	 * @return
	 */
	@RequestMapping("doAddUpload")
	public String doAddUpload(){
		return "userFile/doAddUpload";
	}
	
	/**
	 * 文件上传
	 * @param request
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	@RequestMapping("doUpload")
	@ResponseBody
	public JsonUtil doUpload(HttpServletRequest request, @RequestParam(value="file")MultipartFile file) throws FileNotFoundException{
		String fileName = request.getParameter("fileName");//文件名
		if(StringUtils.isBlank(fileName)) {
			fileName="用户清单";
		}
		String message = request.getParameter("message");//文件描述
		String loginCode=(String)request.getSession().getAttribute("username");//上传名
		String fileSuffixName=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));//获取文件后缀名
		if(!".xlsx".equals(fileSuffixName) && !".xls".equals(fileSuffixName)) {
			throw new HandException("上传失败，文件类型不正确！！");
		}
		String fileType=file.getContentType();//文件类型
		//文件名称不能重复
		List<SysUserFileDTO> list=sysUserFileService.getFileByFileName(fileName);
		if(ListUtil.isNotEmpty(list)){
			throw new HandException("文件名称已经存在，不能继续！");
		}
		try {
			//上传文件
			String nowTime = CodeUtil.getFileCode();
			String uploadDir = FileUtil.DEFAULT_UPLOAD_DIR;
			String resultStr = File.separator + nowTime + File.separator  + fileName+fileSuffixName;
			String uploadFileAllPath = uploadDir + resultStr;
			File restore = new File(uploadFileAllPath);
            if(!restore.getParentFile().exists()){
                restore.getParentFile().mkdirs();
            }
            file.transferTo(restore);
			//保存文件到数据库
			SysUserFileDTO sysUserFileDTO=new SysUserFileDTO();
			sysUserFileDTO.setFileName(fileName);
			sysUserFileDTO.setFilePath(resultStr.replace("\\","/"));
			sysUserFileDTO.setFileType(fileType);
			sysUserFileDTO.setLoginCode(loginCode);
			sysUserFileDTO.setMessage(message);
			sysUserFileDTO.setSuffixName(fileSuffixName);
			sysUserFileService.createFile(sysUserFileDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}		
        return new JsonUtil(true,"上传成功！","");
	}
	
	/**
	 * 执行查询所有角色方法
	 * @return
	 */
	@RequestMapping("/findAllSysUserFileDTO")
	@ResponseBody
	public JsonUtil findAllSysUserFileDTO(SysUserFileDTO SysUserFileDTO){
		List<SysUserFileDTO>  roleDTOList=sysUserFileService.findAllSysUserFileDTO(SysUserFileDTO);
		JsonUtil json=new JsonUtil(true,"发送成功",roleDTOList);
		return json;
	}
	
	@RequestMapping("/deleteFileById")
	@ResponseBody
	public JsonUtil deleteFileById(SysUserFileDTO sysUserFileDTO){
		try {
			sysUserFileService.deleteFileById(sysUserFileDTO.getId(),sysUserFileDTO.getMessage());
			
			return new JsonUtil(true,"删除成功","");
		} catch (Exception e) {
			return new JsonUtil(false,e.getMessage(),"");
		}
	}
	
	@RequestMapping("/findFileById")
	public ModelAndView findFileById(@RequestParam String id,@RequestParam String loginCode,ModelAndView mv){
		mv.setViewName("userFile/findFileById");
		try {
			SysUserFileDTO	SysUserFileDTO = sysUserFileService.findFileById(id);
			mv.addObject("sysUserFileDTO",SysUserFileDTO);
			mv.addObject("loginCode",loginCode);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	@RequestMapping("/updateFileById")
	@ResponseBody
	public JsonUtil updateFileById(SysUserFileDTO SysUserFileDTO){
		try {
			sysUserFileService.updateFileById(SysUserFileDTO);
			return new JsonUtil(true,"更新成功","");
		} catch (Exception e) {
			return new JsonUtil(false,e.getMessage(),"");
		}
	}
	
	
	/**
	 * 文件下载
	 */
	@RequestMapping(value="/downloadFile")
	public void downloadFile(HttpServletResponse res,HttpServletRequest req,SysUserFileDTO sysUserFileDTO){
		SysUserFileDTO file=sysUserFileService.downloadFileById(sysUserFileDTO.getId());
		if(file==null) {
			throw new HandException("文件已经不存在，不能继续！");
		}
		String filePath = FileUtil.DEFAULT_UPLOAD_DIR + file.getFilePath();
		File fileMck = new File(filePath);
        if (!fileMck.exists()) {
        	 throw new HandException("文件已经不存在，不能继续！");
        }
        OutputStream out =null;
		try {
			BufferedInputStream brInputStream = new BufferedInputStream(new FileInputStream(fileMck));
            byte[] buf = new byte[1024];
            int len = 0;
            res.reset();
            // 在线打开方式
            if (false) {
                URL u = new URL("file:///" + filePath);
                res.setContentType(u.openConnection().getContentType());
                res.setHeader("Content-Disposition", "inline; filename=" + URLEncoder.encode(file.getFileName()+file.getSuffixName(), "UTF-8"));
            } else { // 纯下载方式
                res.setContentType("application/x-msdownload");
                res.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(file.getFileName()+file.getSuffixName(), "UTF-8"));
                res.setContentType("application/octet-stream; charset=utf-8");
            }
            out = res.getOutputStream();
            while ((len = brInputStream.read(buf)) > 0){
                out.write(buf, 0, len);
            }
            brInputStream.close();
            out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(out!=null) {
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * 文件导入数据库
	 */
	@RequestMapping(value="/doImportExcel")
	@ResponseBody
	public JsonUtil doImportExcel(HttpServletResponse res,HttpServletRequest req,SysUserFileDTO sysUserFileDTO){
		SysUserFileDTO file=sysUserFileService.downloadFileById(sysUserFileDTO.getId());
		if(file==null) {
			throw new HandException("文件已经不存在，不能继续！");
		}
		String filePath = FileUtil.DEFAULT_UPLOAD_DIR + file.getFilePath();
		File fileMck = new File(filePath);
        if (!fileMck.exists()) {
        	 throw new HandException("文件已经不存在，不能继续！");
        }
        try {
			sysUserFileService.doImportExcel(res,req,fileMck,file.getSuffixName(),sysUserFileDTO.getId());
			return new JsonUtil(true,"入库成功","");
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),"");
		}
	}
	

}
