package zzx.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.RoleDTO;
import zzx.exception.HandException;
import zzx.service.RoleService;
import zzx.util.JsonUtil;

@Controller
@RequestMapping("role")
public class RoleController{
	
	@Resource
	public RoleService roleService;
	
	
	/**
	 * 返回查询所有角色的主页面
	 * @return
	 */
	@RequestMapping("/findAllRoleDTOMain")
	public String findAllRoleDTOMain(){
		return "role/findAllRoleDTOMain";
	}
	/**
	 * 执行查询所有角色方法
	 * @return
	 */
	@RequestMapping("/findAllRoleDTO")
	@ResponseBody
	public JsonUtil findAllRoleDTO(){
		List<RoleDTO>  roleDTOList=roleService.findAllRoleDTO();
		JsonUtil json=new JsonUtil(true,"发送成功",roleDTOList);
		return json;
	}
	/**
	 * 返回新增角色主页面
	 * @return
	 */
	@RequestMapping("/addRoleDTOMain")
	public String addRoleDTOMain(){
		
		return "role/addRoleDTOMain";
	}
	
	/**
	 * 执行新增所有角色方法
	 * @return
	 */
	@RequestMapping("/addRoleDTO")
	@ResponseBody
	public JsonUtil addRoleDTO(RoleDTO roleDTO){
		JsonUtil json=new JsonUtil();
		try {
			roleService.createRoleDTOByRoleDTO(roleDTO);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	/**
	 * 条件查询角色列表
	 * @param roleDTO
	 * @return
	 */
	@RequestMapping("/findRoleDTOByRoleDTO")
	@ResponseBody
	public JsonUtil findRoleDTOByRoleDTO(RoleDTO roleDTO){
		List<RoleDTO> roleDTOList=roleService.findRoleDTOByRoleDTO(roleDTO);
		JsonUtil json =new JsonUtil(true,"查询成功",roleDTOList);
		return json;
	}
	
	
	
	/**
	 * 根据用户id查询用户
	 * @param roleid
	 * @return
	 */
	@RequestMapping("/findRoleDTOByRoleId")
	public ModelAndView findRoleDTOByRoleId(@RequestParam Integer roleid,HttpServletResponse response,ModelAndView mv){
		mv.setViewName("role/findRoleDTOByRoleId");
		try {
			RoleDTO roleDTO=roleService.findRoleDTOByRoleId(roleid);
			mv.addObject("roleDTO", roleDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	/**
	 * 修改一个角色
	 * @param roleDTO
	 * @return
	 */
	@RequestMapping("/updateRoleDTOByRoleId")
	@ResponseBody
	public JsonUtil updateRoleDTOByRoleId(RoleDTO roleDTO){
		JsonUtil json=new JsonUtil();
		try {
			roleService.updateRoleDTOByRoleId(roleDTO);
			json.setMessage("修改成功!");
		} catch (Exception e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 返回分配权限的主页面
	 * @return
	 */
	@RequestMapping("/findAllMenuDTOMain")
	public String findAllMenuDTOMain(@RequestParam Integer roleid){
		return "redirect:/findAllMenuDTOMain.jsp?roleid="+roleid;
	}
	/**
	 * 查询已经启用的所有角色
	 * @param roleDTO 角色实体类里面只有一个参数isused=1
	 * @return
	 */
	@RequestMapping("/findRoleDTOByIsUsed")
	@ResponseBody
	public List<RoleDTO> findRoleDTOByIsUsed(RoleDTO roleDTO){
		List<RoleDTO> roleDTOList=roleService.findRoleDTOByRoleDTO(roleDTO);
		return roleDTOList;
	}
	
	@RequestMapping("/deleteRoleDTOByRoleid")
	@ResponseBody
	public JsonUtil deleteUserDTOByUserId(@RequestParam Integer roleid){
		
		try {
			roleService.deleteRoleDTOByRoleid(roleid);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
}
