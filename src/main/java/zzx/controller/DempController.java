package zzx.controller;


import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import freemarker.template.utility.DateUtil;
import zzx.entity.DempDTO;
import zzx.entity.DeptDTO;
import zzx.exception.HandException;
import zzx.service.DempService;
import zzx.util.CodeUtil;
import zzx.util.JsonUtil;

/**
 * <p>
 *  鍓嶇鎺у埗鍣�
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Controller
@RequestMapping("/dempDTO")
public class DempController {

	@Autowired
    private DempService dempService;
    
    
    /**
	 * 初始页面
	 * @return
	 */
	@RequestMapping("init")
	public String init(){
		return "dempDTO/init";
	}
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listByCondition")
	@ResponseBody
	public JsonUtil listByCondition(DempDTO obj){
		List<DempDTO> list=dempService.selectListByDTO(obj);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	/**
	 * 新增的主页面
	 * @return
	 */
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "dempDTO/addMain";
	}
	
	/**
	 * 新增方法
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(DempDTO obj){
		JsonUtil json=new JsonUtil();
		try {
			if(StringUtils.isBlank(obj.getDeCode())) {
				obj.setDeCode(CodeUtil.getYGCode());
			}
			dempService.saveOne(obj);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"新增成功！",null);
	}
	
	/**
	 * 修改主页面
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("dempDTO/editMain");
		
		Map<String,Object>	map =new HashMap<>();
		try {
			DempDTO obj = dempService.selectById(id);
			SimpleDateFormat  f= new SimpleDateFormat("MM/dd/yyyy");
			obj.setDeTimeStr(f.format(obj.getDeTime()));
			mv.addObject("obj",obj);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 修改方法
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public JsonUtil edit(DempDTO obj){
		JsonUtil json=new JsonUtil();
		try {
			dempService.updateById(obj);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 删除
	 * @param userId
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			dempService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
	
	@RequestMapping("/findEnumDTOByEnumWord")
	@ResponseBody
	public List<DempDTO> findEnumDTOByEnumWord(@RequestParam String enumWord){
		return dempService.selectListByDTO(null);
	} 
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listById")
	@ResponseBody
	public JsonUtil listById(DempDTO obj){
		DempDTO list=dempService.selectById(obj.getId());
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
    
}

