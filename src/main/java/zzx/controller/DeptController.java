package zzx.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import freemarker.template.utility.CollectionUtils;
import zzx.entity.DempDTO;
import zzx.entity.DeptDTO;
import zzx.entity.EnumDTO;
import zzx.exception.HandException;
import zzx.service.DempService;
import zzx.service.DeptService;
import zzx.util.CodeUtil;
import zzx.util.JsonUtil;

/**
 * <p>
 *  鍓嶇鎺у埗鍣�
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Controller
@RequestMapping("/deptDTO")
public class DeptController {

	@Autowired
    private DeptService deptService;
	
	@Autowired
    private DempService dempService;
    
    
    /**
	 * 初始页面
	 * @return
	 */
	@RequestMapping("init")
	public String init(){
		return "deptDTO/init";
	}
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listByCondition")
	@ResponseBody
	public JsonUtil listByCondition(DeptDTO obj){
		List<DeptDTO> list=deptService.selectListByDTO(obj);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	/**
	 * 新增的主页面
	 * @return
	 */
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "deptDTO/addMain";
	}
	
	/**
	 * 新增方法
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(DeptDTO obj){
		JsonUtil json=new JsonUtil();
		try {
			if(StringUtils.isBlank(obj.getDpCode())) {
				obj.setDpCode(CodeUtil.getBMCode());
			}
			deptService.saveOne(obj);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"新增成功！",null);
	}
	
	/**
	 * 修改主页面
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("deptDTO/editMain");
		
		Map<String,Object>	map =new HashMap<>();
		try {
			DeptDTO obj = deptService.selectById(id);
			mv.addObject("obj",obj);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 修改方法
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public JsonUtil edit(DeptDTO obj){
		JsonUtil json=new JsonUtil();
		try {
			deptService.updateById(obj);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 删除
	 * @param userId
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		//通过部门id查询是否在员工中使用
		DempDTO de=new DempDTO();
		de.setDpId(id);
		List<DempDTO> list=dempService.selectListByDTO(de);
		if(!org.springframework.util.CollectionUtils.isEmpty(list)) {
			return new JsonUtil(false,"该部门下还有员工关联，不允许删除！",null);
		}
		try {
			deptService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
	
	@RequestMapping("/findEnumDTOByEnumWord")
	@ResponseBody
	public List<DeptDTO> findEnumDTOByEnumWord(@RequestParam String enumWord){
		return deptService.selectListByDTO(null);
	} 
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/listById")
	@ResponseBody
	public JsonUtil listById(DeptDTO obj){
		DeptDTO list=deptService.selectById(obj.getId());
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
    
}

