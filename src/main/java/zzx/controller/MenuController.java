package zzx.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.MenuDTO;
import zzx.exception.HandException;
import zzx.service.MenuService;
import zzx.util.JsonUtil;

@Controller //这个注解是测试时使用
@RequestMapping("menu")
public class MenuController {
	
	@Resource
	public MenuService menuService;
	
	@RequestMapping("/findAllMenuDTO")
	@ResponseBody
	public JsonUtil findAllMenuDTO(){
		List<MenuDTO> menuDTOList=menuService.findAllMenuDTO();
		JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
		return json;
	}
	
	@RequestMapping("/findParentMenuDTO")
	@ResponseBody
	public JsonUtil findParentMenuDTO(){
		List<MenuDTO> menuDTOList=menuService.findParentMenuDTO();
		JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
		return json;
	}
	
	@RequestMapping("/findChildrenByParentId")
	@ResponseBody
	public JsonUtil findChildrenByParentId(@RequestParam Integer parentId){
		List<MenuDTO> menuDTOList=menuService.findChildrenByParentId(parentId);
		JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
		return json;
	}
	
	@RequestMapping("/findFirstChildrenByParentId")
	@ResponseBody
	public JsonUtil findFirstChildrenByParentId(@RequestParam Integer parentId){
		MenuDTO menuDTOList=menuService.findFirstChildrenByParentId(parentId);
		JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
		return json;
	}
	
	/**
	 * 通过用户id查询所有一级父菜单
	 */
	@RequestMapping("/findParentMenuDTOByUserId")
	@ResponseBody
	public JsonUtil findParentMenuDTOByUserId(@RequestParam Integer userId){
		try {
			List<MenuDTO> menuDTOList = menuService.findParentMenuDTOByUserId(userId);
			JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
			return json;
		} catch (HandException e) {
			e.printStackTrace();
			JsonUtil json=new JsonUtil(false,e.getMessage(),null);
			return json;
		}
		
	}
	/**
	 * 通过用户id查询所有有权限的菜单
	 */
	@RequestMapping("/findAllMenuDTOByUserId")
	@ResponseBody
	public JsonUtil findAllMenuDTOByUserId(@RequestParam Integer userId){
		try {
			List<MenuDTO> menuDTOList = menuService.findAllMenuDTOByUserId(userId);
			JsonUtil json=new JsonUtil(true,"发送成功",menuDTOList);
			return json;
		} catch (HandException e) {
			e.printStackTrace();
			JsonUtil json=new JsonUtil(false,e.getMessage(),null);
			return json;
		}
		
	}
	/**
	 * 返回所有菜单
	 * @return
	 */
	@RequestMapping("/findAllListMenuDTOMain")
	public String findAllListMenuDTOMain(){
		return "redirect:/menu/findAllListMenuDTOMain.jsp";
	}
	
	/**
	 * 返回新增菜单主页面
	 * @return
	 */
	@RequestMapping("/addMenuDTOMain")
	public ModelAndView addMenuDTOMain(@RequestParam Integer id,@RequestParam Integer parentid,HttpServletResponse response,ModelAndView mv){
		mv.setViewName("menu/addMenuDTOMain");
		mv.addObject("parentid", parentid);
		mv.addObject("id", id);
		return mv;
	}
	
	
	
	/**
	 * 返回修改菜单主页面
	 * @return
	 */
	@RequestMapping("/findMenuDTOByMenuId")
	public ModelAndView findMenuDTOByMenuId(@RequestParam Integer id,HttpServletResponse response,ModelAndView mv){
		mv.setViewName("menu/findMenuDTOByMenuId");
		try {
			MenuDTO menuDTO=menuService.findMenuDTOByMenuId(id);
			mv.addObject("menuDTO", menuDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	
	/**
	 * 新增菜单
	 * @param menuDTO
	 * @return
	 */
	@RequestMapping("/addMenuDTO")
	@ResponseBody
	public JsonUtil addMenuDTO(MenuDTO menuDTO){
		JsonUtil json=new JsonUtil();
		try {
			menuService.createMenuDTOByMenuDTO(menuDTO);
			json.setMessage("新增菜单成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 修改菜单
	 * @param meuDTO
	 * @return
	 */
	@RequestMapping("/updateMenuDTOByMenuId")
	@ResponseBody
	public JsonUtil updateMenuDTOByMenuId(MenuDTO menuDTO){
		JsonUtil json=new JsonUtil();
		try {
			menuService.updateMenuDTOByMenuId(menuDTO);
			json.setMessage("修改菜单成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/deleteMenuDTOByMenuId")
	@ResponseBody
	public JsonUtil deleteMenuDTOByMenuId(@RequestParam Integer id){
		
		try {
			menuService.deleteMenuDTOByMenuId(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
	
}
