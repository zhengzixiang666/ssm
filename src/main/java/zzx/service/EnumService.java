package zzx.service;

import java.util.List;

import zzx.entity.EnumDTO;
/**
 * 枚举服务层
 * @author 郑仔祥
 *
 */
public interface EnumService {
	//根据关键字获取枚举列表
	public abstract List<EnumDTO> findEnumDTOByEnumWord(String enumWord);
}
