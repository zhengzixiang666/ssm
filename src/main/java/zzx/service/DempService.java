package zzx.service;

import zzx.entity.DempDTO;
import zzx.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
public interface DempService extends BaseService<DempDTO> {

}
