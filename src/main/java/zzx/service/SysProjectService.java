package zzx.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import zzx.entity.SysProject;

public interface SysProjectService extends BaseService<SysProject>{
	
	public int insertBatch(List<SysProject> list);

	public void deleteByPch(String message);
	
	public void updateBatchByLjh(List<SysProject> list);

	public void doImportExcel(MultipartFile file, String fileSuffixName, String pch)throws Exception;
}
