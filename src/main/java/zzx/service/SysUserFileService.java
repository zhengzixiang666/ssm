package zzx.service;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import zzx.entity.SysUserFileDTO;
import zzx.exception.HandException;

public interface SysUserFileService {
	
	/**
	 * 上传文件
	 * @param SysUserFileDTO
	 */
	void createFile(SysUserFileDTO sysUserFileDTO)throws HandException;
	
	List<SysUserFileDTO> findAllSysUserFileDTO(SysUserFileDTO sysUserFileDTO);
	
	/**
	 * 删除文件
	 * @param id
	 */
	public void deleteFileById(String id,String pch)throws HandException;
	
	/**
	 * 修改文件
	 * @param id
	 */
	public void updateFileById(SysUserFileDTO sysUserFileDTO)throws HandException;
	
	public SysUserFileDTO downloadFileById(String id)throws HandException;
	
	public SysUserFileDTO findFileById(String id)throws HandException;

	List<SysUserFileDTO> getFileByFileName(String fileName);

	void doImportExcel(HttpServletResponse res, HttpServletRequest req, File fileMck,String fileType,String id)throws Exception;
	
}
