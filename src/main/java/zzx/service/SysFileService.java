package zzx.service;

import java.util.List;

import zzx.entity.SysFileDTO;
import zzx.exception.HandException;

public interface SysFileService {
	
	/**
	 * 上传文件
	 * @param sysFileDTO
	 */
	void createFile(SysFileDTO sysFileDTO)throws HandException;
	
	List<SysFileDTO> findAllSysFileDTO(SysFileDTO sysFileDTO);
	
	/**
	 * 删除文件
	 * @param id
	 */
	public void deleteFileById(String id)throws HandException;
	
	/**
	 * 修改文件
	 * @param id
	 */
	public void updateFileById(SysFileDTO sysFileDTO)throws HandException;
	
	public SysFileDTO downloadFileById(String id)throws HandException;
	
	public SysFileDTO findFileById(String id)throws HandException;
	
}
