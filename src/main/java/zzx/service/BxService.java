package zzx.service;

import zzx.entity.BxDTO;
import zzx.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
public interface BxService extends BaseService<BxDTO> {

}
