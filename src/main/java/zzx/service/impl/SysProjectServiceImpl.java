package zzx.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import zzx.dao.SysProjectDAO;
import zzx.entity.SysProject;
import zzx.exception.HandException;
import zzx.service.SysProjectService;
@Service(value="sysProjectService")
public class SysProjectServiceImpl extends BaseServiceImpl<SysProject, SysProjectDAO> implements SysProjectService {

	@Resource
	private SysProjectDAO sysProjectDAO;
	
	
	@Override
	public int insertBatch(List<SysProject> list) {
		
		if(CollectionUtils.isEmpty(list)) {
			return 0;
		}
		
		int num=100;
		List<SysProject> result=new ArrayList<>();
		for (SysProject sysProject : list) {
			result.add(sysProject);
			if(result.size()%num==0) {
				sysProjectDAO.insertBatch(result);
				result=new ArrayList<>();
			}
		}
		if(!CollectionUtils.isEmpty(result)) {
			sysProjectDAO.insertBatch(result);
		}
		return 1;
	}


	@Override
	public void deleteByPch(String message) {
		sysProjectDAO.deleteByPch(message);
		
	}


	@SuppressWarnings("resource")
	@Override
	public void doImportExcel(MultipartFile file, String fileType,String pch) throws Exception {
		InputStream iss=file.getInputStream();
		Sheet sheet=null;
		if(".xls".equals(fileType)) {
			HSSFWorkbook excelxls = new HSSFWorkbook(iss);
			sheet=excelxls.getSheetAt(0);
		}else {
			Workbook excelxlsx= new XSSFWorkbook(iss);
			sheet=excelxlsx.getSheetAt(0);
		}
		//导入表的数据不存在，提示
		if(sheet.getLastRowNum()<1) {
			throw new HandException("表格中没有数据，替换失败！");
		}
		SysProject sysProject=new SysProject();
		sysProject.setPch(pch);
		List<SysProject> list=sysProjectDAO.selectListByDTO(sysProject);//元数据
		Map<String,String> map1=new HashMap<>();
		for (SysProject sysProject2 : list) {
			map1.put(sysProject2.getLjh(), sysProject2.getYhdz());
		}
		//现在的数据
		List<SysProject> listTh=new ArrayList<SysProject>();
		//第一行表头不需要
		for (int runNum = 1; runNum <= sheet.getLastRowNum(); runNum++) {
			Row row = sheet.getRow(runNum);
			int maxColIx = row.getLastCellNum();
			
			Integer[] str=new Integer[]{2,5};
			
			SysProject proj=new SysProject();
			Cell cell=null;
			Map<Integer,String> map=new HashMap<Integer,String>();
			for(int i=0;i<maxColIx;i++) {
				cell=row.getCell(i);
				if(cell==null||cell.toString().equals("")) {
					map.put(i, "");
				}else {
					//统一设置为字符串
					cell.setCellType(CellType.STRING);
					System.out.println(cell.getStringCellValue());
					map.put(i, cell.getStringCellValue());
				}
			}
			proj.setLjh(map.get(str[0]));
			proj.setYhdz(map.get(str[1]));
			listTh.add(proj);
			map.clear();
		}
		Map<String,String> map2=new HashMap<>();
		for (SysProject sysProject2 : listTh) {
			map2.put(sysProject2.getLjh(), sysProject2.getYhdz());
		}
		//对比
		List<SysProject> listGx=new ArrayList<SysProject>();
		for(Map.Entry<String, String> map : map1.entrySet()) {
			if(map2.containsKey(map.getKey())) {
				if(!StringUtils.isBlank(map2.get(map.getKey()))) {
					SysProject s=new SysProject();
					s.setLjh(map.getKey());
					s.setPch(pch);
					s.setYhdz(map2.get(map.getKey()));
					listGx.add(s);
				}
			}
		}
		this.updateBatchByLjh(listGx);
		iss.close();
	}


	@Override
	public void updateBatchByLjh(List<SysProject> list) {
		if(CollectionUtils.isEmpty(list)) {
			return ;
		}
		
		int num=100;
		List<SysProject> result=new ArrayList<>();
		for (SysProject sysProject : list) {
			result.add(sysProject);
			if(result.size()%num==0) {
				sysProjectDAO.updateBatchByLjh(result);
				result=new ArrayList<>();
			}
		}
		if(!CollectionUtils.isEmpty(result)) {
			sysProjectDAO.updateBatchByLjh(result);
		}
		
	}
}
