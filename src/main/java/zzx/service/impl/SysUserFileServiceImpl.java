package zzx.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import zzx.dao.SysUserFileDAO;
import zzx.entity.SysProject;
import zzx.entity.SysUserFileDTO;
import zzx.exception.HandException;
import zzx.service.SysProjectService;
import zzx.service.SysUserFileService;
import zzx.util.CodeUtil;
import zzx.util.ListUtil;
import zzx.util.ZzxUUIDUtil;

@Service(value="sysUserFileService")
public class SysUserFileServiceImpl implements SysUserFileService {
	
	@Resource
	private SysUserFileDAO sysUserFileDAO;
	
	@Resource
	private SysProjectService sysProjectService;
	

	/**
	 * 上传文件
	 */
	@Override
	public void createFile(SysUserFileDTO sysUserFileDTO) {
		
		//文件名不能为空
		if(StringUtils.isBlank(sysUserFileDTO.getFileName())){
			throw new HandException("文件名不能为空，不能继续！");
		}
		//文件后缀名不能为空
		if(StringUtils.isBlank(sysUserFileDTO.getSuffixName())){
			throw new HandException("文件后缀名不能为空，不能继续！");
		}
		//文件类型不能为空
		if(StringUtils.isBlank(sysUserFileDTO.getFileType())){
			throw new HandException("文件类型不能为空，不能继续！");
		}
		//文件字节不能为空
		if(sysUserFileDTO.getFilePath()==null){
			throw new HandException("文件路径不能为空，不能继续！");
		}
		//文件名称不能重复
		List<SysUserFileDTO> list=sysUserFileDAO.getFileByFileName(sysUserFileDTO.getFileName());
		if(ListUtil.isNotEmpty(list)){
			throw new HandException("文件名称已经存在，不能继续！");
		}
		sysUserFileDTO.setId(ZzxUUIDUtil.getUUID());
		sysUserFileDTO.setCreateTime(new Date());
		sysUserFileDAO.uploadFile(sysUserFileDTO);
		
	}

	@Override
	public List<SysUserFileDTO> findAllSysUserFileDTO(SysUserFileDTO SysUserFileDTO) {
		return sysUserFileDAO.getFileList(SysUserFileDTO);
	}

	@Override
	public void deleteFileById(String id,String pch) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		sysUserFileDAO.deleteFileById(id);
		
		sysProjectService.deleteByPch(pch);
	}

	@Override
	public void updateFileById(SysUserFileDTO SysUserFileDTO) throws HandException {
		//id不能为空，文件名不能为空，不能修改库里存在的文件名
		if(StringUtils.isBlank(SysUserFileDTO.getId())){
			throw new HandException("文件id为空，不能继续！");
		}
		//文件名不能为空
		if(StringUtils.isBlank(SysUserFileDTO.getFileName())){
			throw new HandException("文件名不能为空，不能继续！");
		}
		List<SysUserFileDTO> list=sysUserFileDAO.getFileByFileName(SysUserFileDTO.getFileName());
		if(ListUtil.isNotEmpty(list)){
			//名字相同id不同，不是同一个文件
			for (SysUserFileDTO SysUserFileDTO2 : list) {
				if(!SysUserFileDTO2.getId().equals(SysUserFileDTO.getId())){
					throw new HandException("文件名称已经存在，不能继续！");
				}
			}
		}
		//更新
		SysUserFileDTO.setCreateTime(new Date());
		sysUserFileDAO.updateFileById(SysUserFileDTO);
		
	}

	@Override
	public SysUserFileDTO downloadFileById(String id) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		
		return sysUserFileDAO.getFileById(id);
	}

	@Override
	public SysUserFileDTO findFileById(String id) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		return sysUserFileDAO.getFileById(id);
	}

	@Override
	public List<SysUserFileDTO> getFileByFileName(String fileName) {
		
		return sysUserFileDAO.getFileByFileName(fileName);
	}

	@SuppressWarnings("resource")
	@Override
	public void doImportExcel(HttpServletResponse res, HttpServletRequest req, File fileMck,String fileType,String id) throws Exception {
		InputStream is=new  FileInputStream(fileMck);
		Sheet sheet=null;
		if(".xls".equals(fileType)) {
			HSSFWorkbook excelxls = new HSSFWorkbook(is);
			sheet=excelxls.getSheetAt(0);
		}else {
			Workbook excelxlsx= new XSSFWorkbook(is);
			sheet=excelxlsx.getSheetAt(0);
		}
		//导入表的数据不存在，提示
		if(sheet.getLastRowNum()<1) {
			throw new HandException("表格中没有数据，导入失败！");
		}
		String pch=CodeUtil.getCode();
		Date time=new Date();
		List<SysProject> list=new ArrayList<SysProject>();
		//第一行表头不需要
		for (int runNum = 1; runNum <= sheet.getLastRowNum(); runNum++) {
			Row row = sheet.getRow(runNum);
			int maxColIx = row.getLastCellNum();
			
			Integer[] str=new Integer[]{1,2,5,14,15,16,41,45};
			
			SysProject proj=new SysProject();
			Cell cell=null;
			Map<Integer,String> map=new HashMap<Integer,String>();
			for(int i=0;i<maxColIx;i++) {
				cell=row.getCell(i);
				if(cell==null||cell.toString().equals("")) {
					map.put(i, "");
				}else {
					//统一设置为字符串
					cell.setCellType(CellType.STRING);
					map.put(i, cell.getStringCellValue());
				}
			}
			proj.setPch(pch);
			proj.setLxmc(map.get(str[0]));
			proj.setLjh(map.get(str[1]));
			proj.setYhdz(map.get(str[2]));
			proj.setSgr(map.get(str[3]));
			proj.setSgdh(map.get(str[4]));
			proj.setPq(map.get(str[5]));
			proj.setGzyy(map.get(str[6]));
			proj.setGllx(map.get(str[7]));
			proj.setCjrq(time);
			list.add(proj);
			map.clear();
		}
		
		//插表
		if(!CollectionUtils.isEmpty(list)) {
			sysProjectService.insertBatch(list);
		}
		SysUserFileDTO sysUserFile=new SysUserFileDTO();
		sysUserFile.setId(id);
		sysUserFile.setMessage(pch);
		sysUserFileDAO.updateFileById(sysUserFile);
		
		is.close();
	}

	

	
}
