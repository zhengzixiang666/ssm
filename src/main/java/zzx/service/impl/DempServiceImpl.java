package zzx.service.impl;
import org.springframework.stereotype.Service;

import zzx.dao.DempDAO;
import zzx.entity.DempDTO;
import zzx.service.DempService;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Service
public class DempServiceImpl extends BaseServiceImpl<DempDTO,DempDAO> implements DempService {

}
