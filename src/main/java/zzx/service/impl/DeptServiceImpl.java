package zzx.service.impl;
import zzx.dao.DeptDAO;
import zzx.entity.DeptDTO;
import zzx.service.DeptService;
import zzx.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Service
public class DeptServiceImpl extends BaseServiceImpl<DeptDTO,DeptDAO> implements DeptService {

}
