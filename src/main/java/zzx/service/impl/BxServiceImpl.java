package zzx.service.impl;
import org.springframework.stereotype.Service;

import zzx.dao.BxDAO;
import zzx.entity.BxDTO;
import zzx.service.BxService;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@Service
public class BxServiceImpl extends BaseServiceImpl<BxDTO,BxDAO> implements BxService {

}
