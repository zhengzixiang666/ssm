package zzx.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import zzx.dao.UserDAO;
import zzx.dao.UserRoleDAO;
import zzx.entity.RoleDTO;
import zzx.entity.UserDTO;
import zzx.entity.UserRoleDTO;
import zzx.enumutil.BindCodeEnum;
import zzx.enumutil.UserAttrButeEnum;
import zzx.enumutil.YesNoEnum;
import zzx.exception.HandException;
import zzx.service.UserService;
import zzx.util.DateTimeUtil;
import zzx.util.ListUtil;
import zzx.util.StringUtil;
/**
 * 用户服务层实现类
 * @author 郑仔祥
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService{
    
	@Resource
	private UserDAO userDAO;
	
	@Resource
	public UserRoleDAO userRoleDAO;
	
	
	/**
	 * 通过用户id获取用户信息包括角色
	 */
	public Map<String,Object> findUserDTOByUserId(Integer userId)throws HandException{
		if(userId==null){
			throw new HandException("用户id为空，不能继续！");
		}
		Map<String,Object> map=new HashMap<String,Object>();
		//查询用户信息
		UserDTO userDTO= userDAO.getUserDTOByUserId(userId);
		//查询用户绑定的角色id
		String roleid="";
		List<UserRoleDTO> userRoleDTOList=userRoleDAO.getUserRoleDTOByUserId(userId);
		if(ListUtil.isNotEmpty(userRoleDTOList)){
			for (UserRoleDTO userRoleDTO : userRoleDTOList) {
				RoleDTO roleDTO= userRoleDTO.getRoleDTO();
				roleid+=roleDTO.getRoleid()+",";
			}
		}
		if(!StringUtil.isEmpty(roleid)){
			roleid=roleid.substring(0, roleid.lastIndexOf(","));
		}
		map.put("userDTO", userDTO);
		map.put("roleid", roleid);
		return map;
	}
	/**
	 * 创建一个用户
	 */
	public UserDTO createUserDTOByUserDTO(UserDTO userDTO)throws HandException {
		//新用户账号，密码，是否启用，如果创建时间为空则默认给当前系统时间,用户属性默认操作员
		this.checkUserDTO(userDTO);
		//新用户名，不能和数据库中已存在的用户名相同,查询数据库中所有用户
		List<UserDTO> userDTOList=userDAO.getAllUserDTO();
		if(userDTOList!=null && userDTOList.size()>0){
			for (UserDTO userDTO2 : userDTOList) {
				if(userDTO2.getLoginCode().equals(userDTO.getLoginCode())){
					throw new HandException("该用户名已经存在，不能继续！");
				}
			}
		}
		userDTO.setBind(BindCodeEnum.BIND_CODE_2.getValue());
		userDAO.createUserDTO(userDTO);
		return userDTO;
	}
	/**
	 * 删除一个用户，删除一个用户之前要先删除用户角色关联表，再删除用户关联的学生表或老师表
	 */
	public void deleteUserDTOByUserId(Integer userId)throws HandException {
		if(userId==null){
			throw new HandException("用户id为空，不能继续！");
		}
		//根据用户id,查询所有用户关联表中所有这个用户对应的记录
		List<UserRoleDTO> userRoleDTO=userRoleDAO.getUserRoleDTOByUserId(userId);
		if(!ListUtil.isEmpty(userRoleDTO)){
			userRoleDAO.deleteAllUserRoleByUserId(userId);
		}
		userDAO.deleteUserDTO(userId);
	}
	/**
	 * 修改一个用户
	 */
	public void updateUserDTOByUserDTO(UserDTO userDTO,String roleid)throws HandException {
		//基础校验
		this.checkUserDTO(userDTO);
		if(userDTO.getUserId()==null){
			throw new HandException("用户id，为空，不能继续");
		}
		/**
		 * 用户名不能修改为库中已经存在的，除了本身，先判断数据库中用户的id与现在的id不同，然后再
		 * 判断用户名相同则，就不允许修改
		 */
		List<UserDTO> userDTOList=userDAO.getAllUserDTO();
		if(userDTOList==null || userDTOList.size()<=0){
			throw new HandException("目前数据库中暂无用户信息，不能继续！");
		}
		for (UserDTO userDTO2 : userDTOList) {
			if(userDTO2.getUserId()!=userDTO.getUserId() && userDTO2.getLoginCode().equals(userDTO.getLoginCode())){
				throw new HandException("用户名:"+userDTO.getLoginCode()+"已存在，不能继续！");
			}
		}
		//校验通过更新tur_user表
		userDAO.updateUserDTO(userDTO);
		//插入，或更新用户角色关联表
		/**
		 * 如果角色id为空，则根据用户id，查询库里有的角色，删除，库里没有不用删。
		 */
		List<UserRoleDTO> userRoleDTOList=userRoleDAO.getUserRoleDTOByUserId(userDTO.getUserId());
		if(StringUtil.isBlank(roleid)){
			if(ListUtil.isNotEmpty(userRoleDTOList)){
				for (UserRoleDTO userRoleDTO : userRoleDTOList) {
					RoleDTO roleDTO= userRoleDTO.getRoleDTO();
					if(roleDTO!=null && roleDTO.getRoleid()!=null){
						userRoleDAO.deleteUserRoleDTO(userDTO.getUserId(), roleDTO.getRoleid());
					}
				}
			}
		}
		/**
		 * 如果角色id不为空,得到页面传过来的角色id，得到系统库里面的角色id，如果不相同，则删除库里有的，插入传过来的
		 */
		//得到传过来的roleid
		if(StringUtil.isNotBlank(roleid)){
			roleid=roleid+",";
			String[] roles=roleid.split(",");
			//如果库里没有查到这个用户有的角色，则都是新增角色
			if(ListUtil.isEmpty(userRoleDTOList)){
				for (String strid : roles) {
					userRoleDAO.createUserRoleDTO(userDTO.getUserId(), Integer.parseInt(strid));
				}
			}
			//如果库里存在，
			//记录进来有的，库里没有的作为插表
			String jly="";
			//记录进来没有的，库里有的作为删表
			String jlm="";
			if(ListUtil.isNotEmpty(userRoleDTOList)){
				for (UserRoleDTO userRoleDTO : userRoleDTOList) {
					RoleDTO roleDTO= userRoleDTO.getRoleDTO();
					if(roleDTO!=null && roleDTO.getRoleid()!=null){
						Integer rolekl=roleDTO.getRoleid();
						Integer roll=null;
						boolean isex=false;
						if(roles!=null && roles.length>0){
							for (String rol : roles) {
								roll=Integer.parseInt(rol);
								//如果库里有相同的，则不取出
								if(rolekl.compareTo(roll)==0){
									isex=true;
									break;
								}
							}
							if(!isex){
								jlm+=rolekl+",";
							}
						}
					}
				}
				
				
				for (String string : roles) {
					if(!StringUtil.isBlank(string)){
						Integer roljl=Integer.parseInt(string);
						Integer rolkl=null;
						boolean isex=false;
						for (UserRoleDTO userRoleDTO : userRoleDTOList) {
							RoleDTO roleDTO= userRoleDTO.getRoleDTO();
							if(roleDTO!=null && roleDTO.getRoleid()!=null){
								rolkl=roleDTO.getRoleid();
								if(rolkl.compareTo(roljl)==0){
									isex=true;
									break;
								}
							}
						}
						if(!isex){
							jly+=roljl+",";
						}
					}
				}
				
				//记录进来有的，库里没有的作为插表
				if(StringUtil.isNotBlank(jly)){
					String[] jlys= jly.split(",");
					if(jlys!=null && jlys.length>0){
						for (String jlyid : jlys) {
							Integer roleidjly=Integer.valueOf(jlyid);
							userRoleDAO.createUserRoleDTO(userDTO.getUserId(), roleidjly);
						}
					}
				}
				
				//记录进来没有的，库里有的作为删表
				if(StringUtil.isNotBlank(jlm)){
					//删除没有进来库里有的
					String[] jlms= jlm.split(",");
					if(jlms!=null && jlms.length>0){
						for (String jlmid : jlms) {
							Integer roleidjlm=Integer.valueOf(jlmid);
							userRoleDAO.deleteUserRoleDTO(userDTO.getUserId(), roleidjlm);
						}
					}
				}
				
			}
			
		}
		
	}
	/**
	 * 查询所有的用户信息
	 */
	public List<UserDTO> findAllUserDTO() {
		List<UserDTO> userDTOList=userDAO.getAllUserDTO();
		return userDTOList;
	}
	
	public UserDTO findUserDTOById(Integer userId)throws HandException{
		if(userId==null){
			throw new HandException("用户id为空，不能继续！");
		}
		return userDAO.getUserDTOByUserId(userId);
	}
	
	/**
	 * 登陆用户
	 */
	public UserDTO findUserDTOLogin(String username, String password)throws HandException {
		if(StringUtil.isBlank(username)){
			throw new HandException("用户名为空，不能登陆！");
		}
		if(StringUtil.isBlank(password)){
			throw new HandException("密码为空，不能登陆！");
		}
		//根据用户名和密码查询用户，如果不存在，则抛异常
		UserDTO userDTO=userDAO.getUserDTOLogin(username, password);
		if(userDTO==null || userDTO.getUserId()==null){
			throw new HandException("该用户名或者密码错误，不能登陆！");
		}
		return userDTO;
	}
	/**
	 * 条件检索
	 */
	public List<UserDTO> findUserDTOByUserDTO(UserDTO userDTO) {
		List<UserDTO> userDTOList=userDAO.getUserDTOByUserDTO(userDTO);
		return userDTOList;
	}
	
	
	/**
	 * 基础校验 用户账号，密码，是否启用不能为空，如果创建时间为空则默认给当前系统时间,用户属性默认操作员
	 */
	private void checkUserDTO(UserDTO userDTO)throws HandException{
		if(userDTO==null){
			throw new HandException("新用户不存在，不能继续！");
		}
		if(StringUtil.isBlank(userDTO.getLoginCode())){
			throw new HandException("登陆账号为空，不能继续！");
		}
		if(StringUtil.isBlank(userDTO.getLoginPassWord())){
			throw new HandException("登陆密码为空，不能继续！");
		}
		if(userDTO.getIsUsed()==null){
			throw new HandException("是否启用为空，不能继续！");
		}
		if(userDTO.getCreatedate()==null){
			userDTO.setCreatedate(DateTimeUtil.getCurrentTime());
		}
		if(userDTO.getCreatedate()==null){
			throw new HandException("创建日期为空，联系管理员，不能继续！");
		}
		if(userDTO.getAttrBute()==null){
			userDTO.setAttrBute(UserAttrButeEnum.CZY.getValue());
		}
	}
	@Override
	public List<UserDTO> findBindList() {
		
		return userDAO.getUserDTOByBind(BindCodeEnum.BIND_CODE_2.getValue());
	}
	
	@Override
	public void updatePasswordByUserId(UserDTO userDTO) throws HandException {
		if(userDTO==null || userDTO.getUserId()==null){
			throw new HandException("新用户不存在，不能继续！");
		}
		if(StringUtils.isBlank(userDTO.getLoginPassWord())){
			throw new HandException("密码为空，不能继续！");
		}
		userDAO.updateUserDTO(userDTO);
	}
	
	@Override
	public UserDTO registerUserDTOByUserDTO(UserDTO userDTO) throws HandException {
		userDTO.setIsUsed(YesNoEnum.YES.getValue());
		userDTO.setAttrBute("3");
		userDTO.setExt("用户");
		userDTO.setRoleStr("用户");
		userDTO.setCreatedate(new Date());
		userDTO.setBind("1");
		//新用户账号，密码，是否启用，如果创建时间为空则默认给当前系统时间,用户属性默认操作员
		this.checkUserDTO(userDTO);
		//新用户名，不能和数据库中已存在的用户名相同,查询数据库中所有用户
		List<UserDTO> userDTOList=userDAO.getAllUserDTO();
		if(userDTOList!=null && userDTOList.size()>0){
			for (UserDTO userDTO2 : userDTOList) {
				if(userDTO2.getLoginCode().equals(userDTO.getLoginCode())){
					throw new HandException("该用户名已经存在，不能继续！");
				}
			}
		}
		userDTO.setBind(BindCodeEnum.BIND_CODE_2.getValue());
		userDAO.createUserDTO(userDTO);
		//保存用户角色关联表
		userRoleDAO.createUserRoleDTO(userDTO.getUserId(),22);
		return userDTO;
		
	}
}
