package zzx.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import zzx.dao.BaseDAO;
import zzx.exception.HandException;
import zzx.service.BaseService;

public class BaseServiceImpl<T,M extends BaseDAO<T>> implements BaseService<T> {
	
	@Autowired
	private M baseDao;

	@Override
	public List<T> selectListByMap(Map<String, Object> map) {
		return baseDao.selectListByMap(map);
	}

	@Override
	public T selectById(Integer id) throws HandException {
		return baseDao.selectById(id);
	}

	@Override
	public void saveOne(T t) throws HandException {
		baseDao.saveOne(t);
		
	}

	@Override
	public void updateById(T t) throws HandException {
		baseDao.updateById(t);
	}

	@Override
	public void deleteById(Integer id) throws HandException {
		baseDao.deleteById(id);
		
	}

	@Override
	public List<T> selectListByDTO(T t) {
		return baseDao.selectListByDTO(t);
	}

}
