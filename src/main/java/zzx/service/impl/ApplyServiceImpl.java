package zzx.service.impl;
import org.springframework.stereotype.Service;

import zzx.dao.ApplyDAO;
import zzx.entity.ApplyDTO;
import zzx.service.ApplyService;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@Service
public class ApplyServiceImpl extends BaseServiceImpl<ApplyDTO,ApplyDAO> implements ApplyService {

}
