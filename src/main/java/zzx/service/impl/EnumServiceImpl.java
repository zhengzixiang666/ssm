package zzx.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.EnumDAO;
import zzx.entity.EnumDTO;
import zzx.service.EnumService;

@Service("enumService")
public class EnumServiceImpl implements EnumService {
	
	@Resource
	public EnumDAO enumDAO;

	public List<EnumDTO> findEnumDTOByEnumWord(String enumWord) {
		List<EnumDTO> enumDTOList=enumDAO.getEnumDAOByEnumWord(enumWord);
		return enumDTOList;
	}

}
