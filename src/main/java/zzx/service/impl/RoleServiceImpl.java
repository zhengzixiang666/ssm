package zzx.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import zzx.dao.RoleDAO;
import zzx.dao.RoleMenuDAO;
import zzx.dao.UserRoleDAO;
import zzx.entity.RoleDTO;
import zzx.entity.RoleMenuDTO;
import zzx.entity.UserRoleDTO;
import zzx.enumutil.UserAttrButeEnum;
import zzx.exception.HandException;
import zzx.service.RoleService;
import zzx.util.ListUtil;
import zzx.util.StringUtil;
/**
 * 角色服务层
 * @author 郑仔祥
 *
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
	
	@Resource
	public RoleDAO roleDAO;
	
	@Resource
	public RoleMenuDAO roleMenuDAO;
	
	@Resource
	public UserRoleDAO userRoleDAO;

	/**
	 * 获取所有角色
	 * @return
	 */
	public List<RoleDTO> findAllRoleDTO() {
		List<RoleDTO> roleDTOList=roleDAO.getAllRoleDTO();
		return roleDTOList;
	}
	
	/**
	 * 新增用户
	 */
	public RoleDTO createRoleDTOByRoleDTO(RoleDTO roleDTO)throws HandException{
		/**
		 * 角色名不能为空，角色编码不能为空，角色属性为空时默认为操作员3，是否启用不能为空
		 * 逻辑判断，角色名不能相同，角色编码不能相同，
		 * 
		 */
		//先校验空
		checkRoleDTO(roleDTO);
		
		List<RoleDTO> roleDTOList=roleDAO.getAllRoleDTO();
		if(ListUtil.isNotEmpty(roleDTOList)){
			for (RoleDTO role : roleDTOList) {
				if(roleDTO.getCode().equals(role.getCode())){
					throw new HandException("该角色编码已存在，不能继续！");
				}
				if(roleDTO.getName().equals(role.getName())){
					throw new HandException("该角色名称已存在，不能继续！");
				}
			}
		}
		roleDAO.createRoleDTO(roleDTO);
		return roleDTO;
	}
	/**
	 * 根据条件查询角色
	 */
	public List<RoleDTO> findRoleDTOByRoleDTO(RoleDTO roleDTO) {
		List<RoleDTO> roleDTOList=roleDAO.getRoleDTOByRoleDTO(roleDTO);
		return roleDTOList;
	}
	
	/**
	 * 根据角色id查询角色
	 */
	public RoleDTO findRoleDTOByRoleId(Integer roleid)throws HandException {
		if(roleid==null){
			throw new HandException("角色id为空，不能继续！");
		}
		RoleDTO role=roleDAO.getRoleDTOByRoleId(roleid);
		return role;
	}
	
	/**
	 * 更新角色信息
	 */
	public void updateRoleDTOByRoleId(RoleDTO roleDTO) throws HandException{
		/**
		 * 修改角色的逻辑判断
		 * 角色名不能为空，角色编码不能为空，角色属性为空时默认为操作员3，是否启用不能为空
		 * 角色名不能修改为库里面相同，可以不修改，角色编码不能和库里相同，可以不修改，
		 * 不修改就是改为和现在相同的
		 */
		//校验不能为空
		checkRoleDTO(roleDTO);
		List<RoleDTO> roleDTOList=roleDAO.getAllRoleDTO();
		if(ListUtil.isNotEmpty(roleDTOList)){
			for (RoleDTO role : roleDTOList) {
				if(role.getRoleid().compareTo(roleDTO.getRoleid())!=0){
					if(role.getName().equals(roleDTO.getName())){
						throw new HandException("已经存在一个相同的角色名，不能继续！");
					}
					if(role.getCode().equals(roleDTO.getCode())){
						throw new HandException("已经存在一个相同的角色编号，不能继续！");
					}
				}
			}
		}
		roleDAO.updateRoleDTOByRoleId(roleDTO);
		
	}
	
	
	/**
	 * 角色基础校验
	 * @param roleDTO
	 */
	private void checkRoleDTO(RoleDTO roleDTO)throws HandException{
		//先校验空
		if(roleDTO==null){
			throw new HandException("新用户为空，不能继续！");
		}
		if(StringUtil.isBlank(roleDTO.getCode())){
			throw new HandException("角色编码为空，不能继续！");
		}
		if(StringUtil.isBlank(roleDTO.getName())){
			throw new HandException("角色名称为空，不能继续！");
		}
		if(roleDTO.getIsused()==null){
			throw new HandException("是否启用为空，不能继续！");
		}
		if(roleDTO.getAttrbute()==null){
			roleDTO.setAttrbute(Integer.parseInt(UserAttrButeEnum.CZY.getValue()));
		}
	}

	public void deleteRoleDTOByRoleid(Integer roleid) throws HandException {
		if(roleid==null){
			throw new HandException("角色id为空，不能删除！");
		}
		//根据角色id,查询角色菜单关联表
		List<RoleMenuDTO> roleMenuDTOList=roleMenuDAO.getAllRoleMenuDTOByRoleId(roleid);
		if(ListUtil.isNotEmpty(roleMenuDTOList)){
			//如果角色绑定了菜单则先删除关联表
			roleMenuDAO.deleteAllRoleMenuDTOByRoleid(roleid);
		}
		//删除用户角色关联表
		List<UserRoleDTO>  userRoleDTOList=userRoleDAO.getAllUserRoleDTOByRoleId(roleid);
		if(ListUtil.isNotEmpty(userRoleDTOList)){
			userRoleDAO.deleteAllUserRoleByRoleid(roleid);
		}
		//再删除角色表
		roleDAO.deleteRoleDTOByRoleId(roleid);
	}

}
