package zzx.service;

import java.util.List;


import zzx.entity.RoleDTO;
import zzx.exception.HandException;

public interface RoleService {
	
	//查询系统所有角色
	public abstract List<RoleDTO> findAllRoleDTO();
	
	//新增角色
	public  RoleDTO createRoleDTOByRoleDTO(RoleDTO roleDTO)throws HandException;
	
	//根据条件查询角色
	public abstract List<RoleDTO> findRoleDTOByRoleDTO(RoleDTO roleDTO);
	
	public abstract RoleDTO findRoleDTOByRoleId(Integer roleid)throws HandException;
	
	//通过角色id更新角色
	public void updateRoleDTOByRoleId(RoleDTO roleDTO) throws HandException;
	//通过角色id，删除角色，并且删除这个角色所有的菜单，及删除角色菜单关联表
	public void deleteRoleDTOByRoleid(Integer roleid)throws HandException;
	
}
