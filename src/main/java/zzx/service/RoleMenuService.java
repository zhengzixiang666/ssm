package zzx.service;

import java.util.List;

import zzx.entity.MenuDTO;
import zzx.exception.HandException;

public interface RoleMenuService {
	
	public abstract List<MenuDTO> findAllMenuDTOByRoleId(Integer roleid)throws HandException;
	
	/**
	 * 保存或者删除角色菜单表
	 * @param roleid
	 * @param menuids
	 */
	public abstract void saveOrDeleteRoleMenuDTO(Integer roleid,String menuids)throws HandException;

}
