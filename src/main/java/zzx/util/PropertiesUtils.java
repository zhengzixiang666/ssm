package zzx.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import zzx.controller.SysFileController;

public class PropertiesUtils {
	
	public static Object getValueByKey(String key,String propertiesFile){
		Object obj=null;
		Properties  props = new Properties();
		InputStream in=SysFileController.class.getClassLoader().getResourceAsStream(propertiesFile);
		try {
			props.load(in);
			obj=props.get(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
		
	}

}
