package zzx.util;

import java.util.List;

public class ListUtil {
	
	/**
	 * 判断list是否为空
	 * @param list
	 * @return
	 */
	public static boolean isEmpty(List<?> list){
		if(list==null || list.size()<=0 || list.get(0)==null){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 判断list不为空
	 * @param list
	 * @return
	 */
	public static boolean isNotEmpty(List<?> list){
		if(list==null || list.size()==0 || list.get(0)==null){
			return false;
		}else{
			return true;
		}
	}

}
