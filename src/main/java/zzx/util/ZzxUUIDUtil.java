package zzx.util;

import java.util.UUID;

public class ZzxUUIDUtil {
	
	public static String getUUID(){
		String uuid=UUID.randomUUID().toString();
		return uuid.replaceAll("-", "").toUpperCase();
	}

}
