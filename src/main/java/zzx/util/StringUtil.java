package zzx.util;


public class StringUtil {

	/**
	 * 判断是否为空
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str){
		return org.apache.commons.lang3.StringUtils.isBlank(str);
	}
	
	public static boolean isNotBlank(String str){
		return org.apache.commons.lang3.StringUtils.isNotBlank(str);
	}
	
	
	/**
	 *判断是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		if(str==null || str.length()==0 || str.trim()==null || str.trim().length()==0){
			return true;
		}
		return false;
	}
}
