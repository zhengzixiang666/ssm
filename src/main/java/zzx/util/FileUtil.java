package zzx.util;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

public class FileUtil {
	
	public static final String DEFAULT_UPLOAD_DIR="E:\\TEMP\\upload";
	
	/**
	 * 
	 * @param fileName		文件名
	 * @param fileSuffix	文件后缀名
	 * @param res			
	 * @param title
	 * @param dataList
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void doExcel(String fileName,String fileSuffix,HttpServletResponse res,List<String> title,List<Map<Integer,Object>> dataList) throws Exception {
		if(StringUtils.isBlank(fileSuffix)) {
			fileSuffix=".xls";
		}
		//创建HSSFWorkbook对象(excel的文档对象)   
    	HSSFWorkbook wb = new HSSFWorkbook();  
    	//建立新的sheet对象（excel的表单），即工作区间  
    	HSSFSheet sheet=wb.createSheet(fileName);
    	
    	//在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个  
    	HSSFRow rowTitle=sheet.createRow(0);
    	
    	HSSFFont font = wb.createFont();
    	font.setBold(true);
    	HSSFCellStyle cellStyle= wb.createCellStyle();
    	cellStyle.setFont(font);
    	
    	//设置行高
    	rowTitle.setHeight((short)400); 
    	for (int i=0;i<title.size();i++) {
    		HSSFCell cell=rowTitle.createCell(i);
    		cell.setCellType(CellType.STRING);
    		cell.setCellValue(title.get(i));
    		cell.setCellStyle(cellStyle);
    		sheet.setColumnWidth(i,title.get(i).getBytes().length*2*500);
		}
    	
    	for (int i = 0; i < dataList.size(); i++) {
    		HSSFRow row=sheet.createRow(i+1);
    		Map<Integer,Object> map=dataList.get(i);
    		for (int j=0;j<map.size();j++) {
        		HSSFCell cell=row.createCell(j);
        		Object obj=map.get(j);
        		if(obj instanceof String 
        				|| obj instanceof Integer) {
        			cell.setCellType(CellType.STRING);
            		cell.setCellValue(map.get(j)==null?"":String.valueOf(map.get(j)));
        		}
        		if(obj instanceof Double || 
        				obj instanceof BigDecimal) {
        			cell.setCellType(CellType.NUMERIC);
            		cell.setCellValue(Double.valueOf(String.valueOf(map.get(j))));
        		}
        		
    		}
		}
    	//输出Excel文件  
    	OutputStream output = null;
		try {
			res.setContentType("application/x-msdownload");
            res.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName+fileSuffix, "UTF-8"));
            res.setContentType("application/octet-stream; charset=utf-8");
            output = res.getOutputStream();
			wb.write(output);
	    	output.flush();
	    	output.close();
		}  catch (Exception e) {
			e.printStackTrace();
		}  finally {
			if(output!=null) {
				output.close();
			}
		}
	}
	
}
