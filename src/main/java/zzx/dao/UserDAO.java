package zzx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import zzx.entity.UserDTO;
/**
 * 用户DAO接口
 * @author 郑仔祥
 *
 */
@Repository("userDAO")
public interface UserDAO {
	//根据用户名和密码登陆
	public UserDTO getUserDTOLogin(@Param("loginCode")String loginCode,@Param("loginPassWord")String loginPassWord);
	//通过用户id获取用户这个对象
	public UserDTO getUserDTOByUserId(Integer userId);
	//创建一个用户
	public void createUserDTO(UserDTO userDTO);
	//删除一个用户
	public void deleteUserDTO(Integer userId);
	//修改一个用户
	public void updateUserDTO(UserDTO userDTO);
	//查询数据库中所有用户
	public List<UserDTO> getAllUserDTO();
	//根据特定的用户id,查询用户
	public List<UserDTO> getUserDTOSByIds(@Param("ids") List<Long> ids);
	//条件查询
	public abstract List<UserDTO> getUserDTOByUserDTO(UserDTO userDTO);
	
	public List<UserDTO> getUserDTOByBind(String bind);
}
