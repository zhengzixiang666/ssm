package zzx.dao;
 
import org.springframework.stereotype.Repository;

import zzx.entity.BxDTO;
 
/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-12
 */
@Repository
public interface BxDAO extends BaseDAO<BxDTO> {
 
}