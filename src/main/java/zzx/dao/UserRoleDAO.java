package zzx.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import zzx.entity.UserRoleDTO;

@Repository("userRoleDAO")
public interface UserRoleDAO {
	
	/**
	 * 根据用户id查询所有用户角色关联表
	 */
	public abstract List<UserRoleDTO> getAllUserRoleDTOByUserId(Integer userId);
	/**
	 * 根据角色id查询所有用户角色关联表
	 */
	public abstract List<UserRoleDTO> getAllUserRoleDTOByRoleId(Integer roleid);
	
	/**
	 * 根据用户id查所有启用的角色
	 * @param userId
	 * @return
	 */
	public abstract List<UserRoleDTO> getUserRoleDTOByUserId(Integer userId);
	/**
	 * 通过用户id，和角色id创建一个用户角色关联表
	 * @param userid
	 * @param roleid
	 */
	public abstract void createUserRoleDTO(@Param("userid")Integer userid,@Param("roleid")Integer roleid);
	/**
	 * 通过用户id，和角色id，删除一个用户角色关联表
	 * @param userid
	 * @param roleid
	 */
	public abstract void deleteUserRoleDTO(@Param("userid")Integer userid,@Param("roleid")Integer roleid);
	
	/**
	 * 通过用户id，删除所有的用户角色关联表
	 */
	public abstract void deleteAllUserRoleByUserId(@Param("userid")Integer userid);
	
	/**
	 * 通过角色id，删除所有用户角色关联表
	 */
	public abstract void deleteAllUserRoleByRoleid(@Param("roleid")Integer roleid);
}
