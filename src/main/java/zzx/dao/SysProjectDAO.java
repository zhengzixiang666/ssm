package zzx.dao;

import java.util.List;

import zzx.entity.SysProject;

public interface SysProjectDAO extends BaseDAO<SysProject> {
	
	public int insertBatch(List<SysProject> list);

	public void deleteByPch(String pch);
	
	public void updateBatchByLjh(List<SysProject> list);

}
