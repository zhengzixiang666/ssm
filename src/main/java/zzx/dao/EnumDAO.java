package zzx.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import zzx.entity.EnumDTO;

/**
 * 枚举DAO
 * @author 郑仔祥
 *
 */
@Repository("enumDAO")
public interface EnumDAO {
	//根据关键字获取枚举列表
	public abstract List<EnumDTO> getEnumDAOByEnumWord(String enumWord);

}
