package zzx.dao;

import java.util.List;

import zzx.entity.SysUserFileDTO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhengzixiang
 * @since 2019-02-28
 */
public interface SysUserFileDAO {
    
	/**
	 * 文件上传
	 * @param SysUserFileDTO
	 * @return
	 */
	public int uploadFile(SysUserFileDTO sysUserFileDTO);
	
	public List<SysUserFileDTO> getFileList(SysUserFileDTO sysUserFileDTO);
	
	public SysUserFileDTO getFileById(String id);
	
	public List<SysUserFileDTO> getFileByFileName(String fileName);
	
	public void deleteFileById(String id);
	
	public void updateFileById(SysUserFileDTO sysUserFileDTO);
	
}
