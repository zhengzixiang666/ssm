package zzx.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import zzx.entity.RoleDTO;

/**
 * 角色DAO
 * @author 郑仔祥
 *
 */
@Repository("roleDAO")
public interface RoleDAO {
	
	//查询所有角色
	public abstract List<RoleDTO> getAllRoleDTO();
	//新增角色
	public abstract void createRoleDTO(RoleDTO roleDTO);
	
	//根据条件查询角色
	public abstract List<RoleDTO> getRoleDTOByRoleDTO(RoleDTO roleDTO);
	
	//根据角色id,查询该角色
	public abstract RoleDTO getRoleDTOByRoleId(Integer roleid);
	
	//修改角色
	public abstract void updateRoleDTOByRoleId(RoleDTO roleDTO);
	//通过角色id，删除一个角色
	public abstract void deleteRoleDTOByRoleId(Integer roleid);

}
