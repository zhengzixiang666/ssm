package zzx.dao;
 
import org.springframework.stereotype.Repository;

import zzx.entity.DempDTO;
 
/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Repository
public interface DempDAO extends BaseDAO<DempDTO> {
 
}