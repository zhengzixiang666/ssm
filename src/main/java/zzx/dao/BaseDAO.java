package zzx.dao;

import java.util.List;
import java.util.Map;

public interface BaseDAO<T> {
	
	/**
	 * 查询，带条件
	 */
	public List<T> selectListByMap(Map<String,Object> map);
	
	/**
	 * 查询，带条件
	 */
	public List<T> selectListByDTO(T t);
	
	/**
	 * 通过id查询
	 */
	public T selectById(Integer id);
	
	/**
	 * 增加
	 */
	public void saveOne(T t);
	
	/**
	 * 修改
	 * 
	 */
	public void updateById(T t);
	
	/**
	 * 删除
	 * 
	 */
	public void deleteById(Integer id);
	
}
