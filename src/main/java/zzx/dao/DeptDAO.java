package zzx.dao;
 
import org.springframework.stereotype.Repository;

import zzx.entity.DeptDTO;
 
/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhengzx
 * @since 2021-12-11
 */
@Repository
public interface DeptDAO extends BaseDAO<DeptDTO> {
 
}