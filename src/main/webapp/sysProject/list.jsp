<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文件生成</title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}
//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '文件id',
				checkbox:  true
			},{
				field : 'pch',
				title : '批次号',
				width : '15%',
				align : 'center'
			},{
				field : 'lxmc',
				title : '类型名称',
				width : '10%',
				align : 'center'
			}, {
				field : 'ljh',
				title : '逻辑号',
				width : '8%',
				align : 'center'
			}, {
				field : 'yhdz',
				title : '用户地址',
				width : '20%',
				align : 'center'
			}, {
				field : 'sgr',
				title : '施工人',
				width : '15%',
				align : 'center'
			}, {
				field : 'sgdh',
				title : '施工电话',
				width : '10%',
				align : 'center'
			}, {
				field : 'pq',
				title : '片区',
				width : '12%',
				align : 'center'
			}, {
				field : 'gzyy',
				title : '故障原因',
				width : '15%',
				align : 'center'
			}, {
				field : 'gllx',
				title : '光网络箱',
				width : '10%',
				align : 'center'
			},{
				field : 'cjrq',
				title : '日期',
				width : '10%',
				align : 'center',
				formatter : function(value){
	                var date = new Date(value);
	               return formatDate(date,"yyyy-MM-dd");
	            }
			}]]
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'select.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(data){
				var json=data.data;
				alert(json.message);
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		$('#dg').datagrid('loadData',json);
	}
	function doQuerySort(){
		var sgr=jQuery('#sgr').val();
		var sgdh=jQuery('#sgdh').val();
		var gzyy=jQuery('#gzyy').val();
		var pq=jQuery('#pq').val();
		var ljh=jQuery('#ljh').val();
		var sort=1;
		$.ajax({
			url:'select.do',
			type:'post',
			data:{"sgr":sgr,"sgdh":sgdh,"gzyy":gzyy,"pq":pq,"sort":sort,"ljh":ljh},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(data){
				var json=data.data;
				alert(json.message);
			}
		});
	}
	//查询
	function doQuery(){
		var sgr=jQuery('#sgr').val();
		var sgdh=jQuery('#sgdh').val();
		var gzyy=jQuery('#gzyy').val();
		var pq=jQuery('#pq').val();
		var ljh=jQuery('#ljh').val();
		$.ajax({
			url:'select.do',
			type:'post',
			data:{"sgr":sgr,"sgdh":sgdh,"gzyy":gzyy,"pq":pq,"ljh":ljh},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(data){
				var json=data.data;
				alert(json.message);
			}
		});
	}
	function doExcel(){
		var sgr=jQuery('#sgr').val();
		var sgdh=jQuery('#sgdh').val();
		var gzyy=jQuery('#gzyy').val();
		var pq=jQuery('#pq').val();
		var path=window.parent.$('#path').val();
		window.location.href=path+"/sysProject/doExcel.do?sgr="+sgr+"&sgdh="+sgdh+"&gzyy="+gzyy+"&pq="+pq;
	}
	
	function doImport(){
		layer.open({
			type:2,
			title:'当前位置 : 上传文件>>匹配地址',
			content:'doAddUpload.do',
			skin:'layui-layer-lan',
			area:['400px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				layer.close(index);
			},
			cancel: function(index, layero){ 
				 layer.close(index);
			}   
		});
	}
	
</script>
</head>
<body>
	    <div id="tb" style="padding:15px;background-color: #ADD8E6;">
				<div>
					当前位置：文件明细
				</div>
				<div style="padding:15px">
					<span style="margin-left: 5%">施工人:</span>
					<input id="sgr" class="easyui-textbox" type="text">
					<span style="margin-left: 5%">施工人电话:</span>
					<input id="sgdh" class="easyui-textbox" type="text">
					<span style="margin-left: 5%">故障原因:</span>
					<input id="gzyy" class="easyui-combobox" panelHeight="300px;" name="gzyy" 
    						data-options="valueField:'code',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=gzyy'"></input>
					<div style="margin-top: 20px;">
						<span style="margin-left: 5%">逻辑号:</span>
						<input id="ljh" class="easyui-textbox" type="text">
						<span style="margin-left: 5%">片区&nbsp;&nbsp;&nbsp;&nbsp;:</span>
						<input id="pq" class="easyui-textbox" type="text">
						<a href="#" class="easyui-linkbutton" style="margin-left:5%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
						<a href="#" class="easyui-linkbutton" style="margin-left:20px;" iconCls="icon-man" onclick="doExcel()">导 出</a>
						<a href="#" class="easyui-linkbutton" style="margin-left:20px;" iconCls="icon-search" onclick="doQuerySort()">排序查</a>
						<a href="#" class="easyui-linkbutton" style="margin-left:20px;" iconCls="icon-man" onclick="doImport()">匹配地址</a>
					</div>
				</div>
		</div>
		<table id="dg">
		</table>
</body>
</html>