<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改用户</title>
<script type="text/javascript">
	//执行更新方法
	function doUpdate(){
			$('#form').form('submit',{
				url:'updateRoleDTOByRoleId.do',
				success:function(data){
					var data = eval('(' + data + ')');//将json对象转为js对象
					layer.alert(data.message,{
						skin: 'layui-layer-molv'
					});
				}
			});
	}
	
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    		<tr>
		    			<td><input type="hidden" id="roleid" name="roleid" value="${roleDTO.roleid}"/></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>角色名称：<input id="name" name="name" value="${roleDTO.name}" class="easyui-textbox" type="text" ></input></label></td>
		    			<td align="right"><label>角色编码：<input id="code" name="code" value="${roleDTO.code}" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>是否启用：<input id="isused" name="isused" value="${roleDTO.isused}" class="easyui-combobox"  panelHeight="auto"
		    			 data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isused'"></input></label></td>
		    			<td align="right"><label>备注：<input id="remark" name="remark" value="${roleDTO.remark}" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td colspan="2"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">更 新</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>