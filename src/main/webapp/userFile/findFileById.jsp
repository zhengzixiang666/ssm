<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文件生成</title>
<script type="text/javascript">

	function doUpdate(){
		$("#form").form("submit", {
			    type: 'post',
			    url: 'updateFileById.do',
			    onSubmit: function (param) {
				//这里可以增加额外的参数
// 					console.log(param.bdatajson);
// 					param.bdatajson = "zzx";
			    },
			    success: function (data) {
					var data = eval('(' + data + ')');//将json对象转为js对象
					layer.alert(data.message,{
						skin: 'layui-layer-molv',
						yes:function(index, layero){
							window.parent.layer.close(index);//关闭父窗口
							window.parent.location.reload();//刷新父窗口
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" enctype="multipart/form-data">
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><input id="id" name="id" value="${sysUserFileDTO.id}" class="easyui-textbox" type="hidden" ></input></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><input id="loginCode" name="loginCode" value="${loginCode}" class="easyui-textbox" type="hidden" ></input></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>文件名称：<input id="fileName" name="fileName" value="${sysUserFileDTO.fileName}" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
<!-- 		    		<tr> -->
<%-- 		    			<td align="right"><label>文件描述：<input id="message" name="message" value="${sysUserFileDTO.message}" class="easyui-textbox" type="text" ></input></label></td> --%>
<!-- 		    		</tr> -->
		    		<tr>
		    			<td colspan="2"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">修改</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>