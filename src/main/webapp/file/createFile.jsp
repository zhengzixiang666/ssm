<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文件生成</title>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/ribbon/ribbon.css"/>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/ribbon/ribbon-icon.css"/>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/texteditor/texteditor.css"/>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/ribbon/jquery.ribbon.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/texteditor/jquery.texteditor.js"></script>
<script type="text/javascript">

	

</script>
</head>
<body>
	<div class="easyui-texteditor" title="文本编辑" style="width:100%px;height:500px;padding:20px"></div>
</body>
</html>