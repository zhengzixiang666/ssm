<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ 
			 			 		{
						field : 'id',
						title : '主键',
						checkbox:  true
					},
		      		        	  	{
							field : 'bxCode',
							title : '报销编号',
							width : '20%',
							align : 'center'
						},
				      	
		      		        	  	{
							field : 'userId',
							title : '申请人',
							width : '10%',
							align : 'center',
							formatter : function(value){
								var result="";
								$.ajax({
			            			url:'<%= this.getServletContext().getContextPath()%>/dempDTO/listById.do',
			            			type:'post',
			            			data:{"id":value},
			            			dataType:'json',
			            			async:false,
			            			success:function(data){
			            				var json=data.data;
			            				result=json.deName;
			            			},
			            			error:function(){
			            				alert('error');
			            			}
			            		});
			                   return result;
			                }
						},
				      	
		      		        	  	{
							field : 'bxType',
							title : '报销类型',
							width : '10%',
							align : 'center',
							formatter : function(value){
								var result="";
								 $.ajax({
				            			url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do',
				            			type:'get',
				            			data:{"enumWord":"applyStatus"},
				            			dataType:'json',
				            			async:false,
				            			success:function(data){
				            				var json=data;
				            				for(var i=0;i<json.length;i++){
				            					if(value==json[i].otherid){
				            						result=json[i].name;
				            						break;
				            					}
				            				}
				            			},
				            			error:function(){
				            				alert('error');
				            			}
				            		});
			                   return result;
			                }
						},
				      	
		      		        	  	{
							field : 'money',
							title : '金额',
							width : '10%',
							align : 'center'
						},
				      	
		      		        	  	{
							field : 'applyTime',
							title : '申请时间',
							width : '15%',
							align : 'center',
							formatter : function(value){
			                    var date = new Date(value);
			                   return formatDate(date,"yyyy-MM-dd HH:mm:ss");
			                }
						},
				      	
		      		        	  	{
							field : 'applyStatus',
							title : '状态',
							width : '10%',
							align : 'center',
							formatter : function(value){
								var result="";
								 $.ajax({
				            			url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do',
				            			type:'get',
				            			data:{"enumWord":"applyStatus"},
				            			dataType:'json',
				            			async:false,
				            			success:function(data){
				            				var json=data;
				            				for(var i=0;i<json.length;i++){
				            					if(value==json[i].otherid){
				            						result=json[i].name;
				            						break;
				            					}
				            				}
				            			},
				            			error:function(){
				            				alert('error');
				            			}
				            		});
			                   return result;
			                }
						},
				      	
		      				      	
		      {
				field : 'field',
				title : '操作',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					if(row.applyStatus==2){
						var data=" <a href='#' name='search' onclick='searchs(" + row.id + ")' title='查看'></a> "+
						"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.id + ")' title='删除'></a>";
						return data;
					}else{
						var data=" <a href='#' name='edit' onclick='edit(" + row.id + ")' title='修改'></a> "
						+ "\| <a href='#' name='man' class='easyui-linkbutton' onclick='man(" + row.id + ")' title='提交'></a> "
						+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.id + ")' title='删除'></a>";
						return data;
					}
				}
				
			}]],
			onLoadSuccess:function(data){    
			     $("a[name='search']").linkbutton({text:'查看',plain:false,iconCls:'icon-search'}); 
			     $("a[name='man']").linkbutton({text:'提交',plain:false,iconCls:'icon-man'});
		        $("a[name='edit']").linkbutton({text:'修改',plain:false,iconCls:'icon-edit'}); 
		        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		var userId=null;
		var bxType=jQuery('#bxType').val();
  		var applyStatus=jQuery('#applyStatus').val();
		var currentUserId=jQuery('#currentUserId').val();
		if(currentUserId && (currentUserId!=16 && currentUserId!=38)){
				userId=currentUserId;
				
			}
		var data={
				"userId":userId,
    	  		"bxType":bxType,
    	  		"applyStatus":applyStatus,
};
		$.ajax({
			url:'listByCondition.do',
			type:'post',
			data:data,
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		for ( var i in json) {
			if(json[i].isUsed==1 || json[i].isUsed=='1'){
				json[i].isUsed='启用';
			}
			if(json[i].isUsed==2 || json[i].isUsed=='2'){
				json[i].isUsed='不启用';
			}
			if(json[i].attrBute==1){
				json[i].attrBute='超级管理员';
			}
			if(json[i].attrBute==2){
				json[i].attrBute='管理员';
			}
			if(json[i].attrBute==3){
				json[i].attrBute='操作员';
			}
		}
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var userId=null;
		var bxType=jQuery('#bxType').val();
  		var applyStatus=jQuery('#applyStatus').val();
		var currentUserId=jQuery('#currentUserId').val();
		if(currentUserId && (currentUserId!=16 && currentUserId!=38)){
				userId=currentUserId;
				
			}
		var data={
				"userId":userId,
    	  		"bxType":bxType,
    	  		"applyStatus":applyStatus,
};
		$.ajax({
			url:'listByCondition.do',
			type:'post',
			data:data,
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//新增
	function doAdd(){
		layer.open({
			type:2,
			title:'当前位置 : 新增',
			content:'addMain.do',
			skin:'layui-layer-lan',
			area:['600px','500px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
					layer.close(index)
				}   
			});   
	}
	//查看
	function searchs(id){
		layer.open({
			type:2,
			title:'当前位置 : 查看',
			content:'searchMain.do?id='+id,
			skin:'layui-layer-lan',
			area:['600px','500px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
				  layer.close(index)
				}   
			}); 
	}
	function man(id){
		layer.alert("确认要提交吗？”，点击确认，提交申请!",{
			skin: 'layui-layer-molv',
			yes:function(){
				$.ajax({
					type:'post',
					data:{"id":id,"applyStatus":"2"},
					url:'edit.do',
					dataType:'json',
					success:function(json){
						reloadData();
						layer.alert(json.message);
					},
					error:function(){
						layer.alert("提交失败");
					}
				});
			}
		});
	}
	//修改方法
	function edit(id){
		layer.open({
			type:2,
			title:'当前位置 : 修改',
			content:'editMain.do?id='+id,
			skin:'layui-layer-lan',
			area:['600px','500px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
				  layer.close(index)
				}   
			}); 
	}

	//删除方法
	function deleteRow(id){
	layer.alert("确认要删除吗？”，点击确认，删除数据!",{
			skin: 'layui-layer-molv',
			yes:function(){
				$.ajax({
					type:'post',
					data:{"id":id},
					url:'delete.do',
					dataType:'json',
					success:function(json){
						reloadData();
						layer.alert(json.message);
					},
					error:function(){
						layer.alert("删除失败");
					}
				});
			}
		});
	}
	
	
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：报销管理
			</div>
			<div style="padding:15px">
								<input type="hidden" id="currentUserId" name="currentUserId" value="${user.dempId}"/>
						        <span style="margin-left: 10px">报销类型:</span>
								<input id="bxType" class="easyui-combobox" panelHeight="auto" name="bxType" 
				    			data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=bxType'"></input>
						        <span style="margin-left: 10px">状态:</span>
								<input id="applyStatus" class="easyui-combobox" panelHeight="auto" name="applyStatus" 
				    			data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=applyStatus'"></input>
				<a href="#" class="easyui-linkbutton" style="margin-left:20%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
				<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doAdd()">新 增</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>