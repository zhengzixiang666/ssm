<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doUpdate(){
		$('#form').form('submit',{
			url:'edit.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    	     <tr>
		    			<td><input type="hidden" id="id" name="id" value="${obj.id}"/></td>
		    		</tr>
		    							                 <tr>
							    			<td align="right"><label>报销编号：<input id="bxCode" disabled value="${obj.bxCode}" name="bxCode"  class="easyui-textbox" type="text" style="width:400px"></input></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>申请人：<input id="userId" disabled value="${obj.userId}" name="userId" class="easyui-combobox"  panelHeight="auto" style="width:400px" 
    											data-options="valueField:'id',textField:'deName',url:'<%= this.getServletContext().getContextPath()%>/dempDTO/findEnumDTOByEnumWord.do?enumWord=userId'"></input></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>报销类型：<input id="bxType" disabled value="${obj.bxType}" name="bxType" class="easyui-combobox"  panelHeight="auto" style="width:400px"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=bxType'"></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>金额：<input id="money" disabled value="${obj.money}" name="money"  class="easyui-textbox" type="text"  style="width:400px"></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>申请时间： <input class="easyui-datetimebox" disabled value="${obj.applyTime}" id="applyTime" name="applyTime" style="width:400px"
                                           data-options="" ></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>状态：<input id="applyStatus" disabled value="${obj.applyStatus}" name="applyStatus" class="easyui-combobox"  panelHeight="auto" style="width:400px"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=applyStatus'"></input></label></td>
							    		</tr>
							    		<tr>
							    			<td align="right"><label>摘要：<input id="bxDesc" value="${obj.bxDesc}" name="bxDesc" disabled data-options="multiline:true" style="width:400px;height:100px" class="easyui-textbox" type="text" ></input></label></td>
							    		</tr>
		    		
		    	</table>
		   </form>
	   </div>
</body>
</html>