<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改用户</title>
<script type="text/javascript">
	//执行更新方法
	function doUpdate(){
			$('#form').form('submit',{
				url:'updateMenuDTOByMenuId.do',
				success:function(data){
					var data = eval('(' + data + ')');//将json对象转为js对象
					layer.alert(data.message,{
						skin: 'layui-layer-molv',
						yes:function(index, layero){
							window.parent.layer.close(index);
							window.parent.location.reload();//刷新父窗口
						}
					});
					
				}
			});
	}
	
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td><input id="id" name="id" value="${menuDTO.id}" style="width: 260px"  type="hidden" ></input></td>
		    		</tr>
		    		<tr>
		    			<td><input id="parentid" name="parentid" value="${menuDTO.parentid}" style="width: 260px"  type="hidden" ></input></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单名称：<input id="text" name="text" value="${menuDTO.text}" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单类型：<input id="isleaf" name="isleaf" value="${menuDTO.isleaf}" style="width: 260px" class="easyui-combobox"  panelHeight="auto"
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isleaf'"></input></label></td>
		    		</tr>
		    		
		    		<tr>
		    			<td align="right"><label>菜单url：<input id="url" name="url" value="${menuDTO.url}" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单图标：<input id="iconCls" name="iconCls" value="${menuDTO.iconCls}" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单顺序：<input id="menuorder" name="menuorder" value="${menuDTO.menuorder}" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>备注：<input id="remark" name="remark" value="${menuDTO.remark}" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td colspan="1"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">更 新</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>