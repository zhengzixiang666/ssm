<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doSave(){
		$('#form').form('submit',{
			url:'addMenuDTO.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(index, layero){
						window.parent.layer.close(index);//关闭父窗口
						window.parent.location.reload();//刷新父窗口
					}
				});
			}
		});
	}
	//如果为菜单则需要掩藏url
	$(document).ready(function () {
		$("#isleaf").combobox({
				onChange: function (n,o) {
					var isleaf=$("#isleaf").val();
					if(isleaf==2){
						$('#hiddenUrl').hide();
					}else{
						$('#hiddenUrl').show();
					}
				}
			});
	});
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td><input id="parentid" value="${param.parentid}" type="hidden" name="parentid" /></td>
		    			<td><input id="id" value="${param.id}" type="hidden" name="id" /></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单名称：<input id="text" name="text" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单类型：<input id="isleaf" name="isleaf" style="width: 260px" class="easyui-combobox"  panelHeight="auto" 
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isleaf'"></input></label></td>
		    		</tr>
		    		
		    		<tr>
		    			<td align="right" id="hiddenUrl"><label>菜单url：<input id="url" name="url" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单图标：<input id="iconCls" name="iconCls" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>菜单顺序：<input id="menuorder" name="menuorder" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>备注：<input id="remark" name="remark" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div>
							    <label for="name">节点:   
							    <input type="radio" class="sex" name="brotherOrSon" value="1" checked="checked">兄弟
							    <input type="radio" class="sex" name="brotherOrSon" value="2">子节点
								</label>
							</div>
		    			</td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">保 存</a> </div>
		    			</td>
		    		</tr>
		    	</table>
		    	
		   </form>
	   </div>
</body>
</html>