<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doSave(){
		$('#form').form('submit',{
			url:'add.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
		
	}
// 	function format(date){
// 		 return formatDate(date,"yyyy-MM-dd");
// 	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    							<tr>
							    			<td align="right"><label>请假编号：<input id="qjCode" name="qjCode"  class="easyui-textbox" type="text"  style="width:400px"></input></label></td>
							    		</tr>
<!-- 		    							<tr> -->
<!-- 							    			<td align="right"><label>申请人：<input id="userId" name="userId" class="easyui-combobox"  panelHeight="auto" -->
<%--     											data-options="valueField:'id',textField:'deName',url:'<%= this.getServletContext().getContextPath()%>/dempDTO/findEnumDTOByEnumWord.do?enumWord=userId'" style="width:400px"></input></label></td> --%>
<!-- 							    		</tr> -->
								         <tr>
							    			<td align="right"><label>请假类型：<input id="qjType" name="qjType" class="easyui-combobox"  panelHeight="auto"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=qjType'" style="width:400px"></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>请假事由：<input id="qjDesc" name="qjDesc"  class="easyui-textbox" type="text" data-options="multiline:true" style="width:400px;height:100px"></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>开始时间： <input class="easyui-datetimebox" id="startTime" name="startTime"  style="width:400px"
                                           data-options="" ></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>结束时间： <input class="easyui-datetimebox" id="endTime" name="endTime"  style="width:400px"
                                           data-options="" ></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>申请状态：<input id="applyStatus" name="applyStatus" class="easyui-combobox"  style="width:400px" panelHeight="auto"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=applyStatus'"></input></label></td>
							    		</tr>
<!-- 						                 <tr> -->
<!-- 							    			<td align="right"><label>提交时间： <input class="easyui-datetimebox" id="submitTime" name="submitTime" -->
<!--                                            data-options="" ></label></td> -->
<!-- 							    		</tr> -->
				      	<tr>
		    			<td ><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">保 存</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>