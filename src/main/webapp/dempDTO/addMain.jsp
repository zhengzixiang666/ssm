<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doSave(){
		$('#form').form('submit',{
			url:'add.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
		
	}
// 	function format(date){
// 		 return formatDate(date,"yyyy-MM-dd");
// 	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    	
		    									                 <tr>
							    			<td align="right"><label>员工编号：<input id="deCode" name="deCode"  class="easyui-textbox" type="text" ></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>员工姓名：<input id="deName" name="deName"  class="easyui-textbox" type="text" ></input></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>性别：<input id="deSex" name="deSex" class="easyui-combobox"  panelHeight="auto"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=deSex'"></input></label></td>
							    		</tr>
								         <tr>
							    			<td align="right"><label>所属部门：<input id="dpId" name="dpId" class="easyui-combobox"  panelHeight="auto"
    											data-options="valueField:'id',textField:'dpName',url:'<%= this.getServletContext().getContextPath()%>/deptDTO/findEnumDTOByEnumWord.do?enumWord=dpId'"></input></label></td>
							    		</tr>
						                 <tr>
							    			<td align="right"><label>入职时间： <input class="easyui-datebox" id="deTime" name="deTime"
                                           data-options="showSeconds:false" ></label></td>
							    		</tr>
				      	<tr>
		    			<td ><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">保 存</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>